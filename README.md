# README #

This project provides an API specific for PharmML 0.8.1.

Developed by Stuart Moodie on behalf of the DDMoRe Foundation.

Copyright 2017.

## What is this repository for? ##

### Quick summary ###

The current libpharmml API tries to achieve backwards compatibility with all versions of PharmML.  Unfortunately the changes between later versions mean that it is no longer possible to support PharmML 0.8.1 and earlier versions.  This has led to a situation where version 0.7.3-1 cannot be used in code that needs to support versions of PharmML earlier than 0.8.  To support earlier versions you need to use libpharmml 6.x.

This library has been created to address this problem by providing an API that specifically only supports PharmML 0.8.1 and which has a different name and maven artefact id.  It can be used in addition to libpharmml and when used with libPharmML 0.6.x software tools ca be developed that support the latest and legacy PharmML documents.

### Version ###
1.0.0-SNAPSHOT

## How do I get set up? ##

### Summary of set up ###

The project is built with maven. So to install:

mvn clean install