/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.impl;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by stumoodie on 11/05/2017.
 */
public class DefaultNamespaceMapper  extends NamespacePrefixMapper {
    static final public String NS_DEFAULT_CT = "http://www.pharmml.org/pharmml/0.8/CommonTypes";
    static final public String NS_DEFAULT_DS = "http://www.pharmml.org/pharmml/0.8/Dataset";
    static final public String NS_DEFAULT_MATH = "http://www.pharmml.org/pharmml/0.8/Maths";
    static final public String NS_DEFAULT_MDEF = "http://www.pharmml.org/pharmml/0.8/ModelDefinition";
    static final public String NS_DEFAULT_TD = "http://www.pharmml.org/pharmml/0.8/TrialDesign";
    static final public String NS_DEFAULT_MSTEPS = "http://www.pharmml.org/pharmml/0.8/ModellingSteps";
    static final public String NS_DEFAULT_MML = "http://www.pharmml.org/pharmml/0.8/PharmML";
    static final public String NS_DEFAULT_UNCERTML = "http://www.uncertml.org/3.0";
    static final public String NS_DEFAULT_PROBONTO = "http://www.pharmml.org/probonto/ProbOnto";

    private Map<String, String> namespaceMap = new HashMap<>();

    /**
     * Create mappings.
     */
    public DefaultNamespaceMapper(){
        namespaceMap.put("http://www.w3.org/2001/XMLSchema-instance", "xsi");
        namespaceMap.put(NS_DEFAULT_MML, "");
        namespaceMap.put(NS_DEFAULT_CT, "ct");
        namespaceMap.put(NS_DEFAULT_DS, "ds");
        namespaceMap.put(NS_DEFAULT_MATH, "math");
        namespaceMap.put(NS_DEFAULT_MDEF, "mdef");
        namespaceMap.put(NS_DEFAULT_TD, "td");
        namespaceMap.put(NS_DEFAULT_MSTEPS, "msteps");
        namespaceMap.put(NS_DEFAULT_UNCERTML, "uncertml");
        namespaceMap.put(NS_DEFAULT_PROBONTO, "probonto");
    }

    /* (non-Javadoc)
     * Returning null when not found based on spec.
     * @see com.sun.xml.bind.marshaller.NamespacePrefixMapper#getPreferredPrefix(java.lang.String, java.lang.String, boolean)
     */
    @Override
    public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
        String prefix = namespaceMap.get(namespaceUri);
        if(prefix == null){
            prefix = suggestion;
        }

        return prefix;
    }
}
