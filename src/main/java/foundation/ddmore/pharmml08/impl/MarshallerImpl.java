/*******************************************************************************
 * Copyright (c) 2013 European Molecular Biology Laboratory,
 * Heidelberg, Germany.
 *
 * Copyright (c) 2017 DDMoRe Foundation,
 * Utrecht, NL.
 *
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in
 * compliance with the License.  You may obtain a copy of
 * the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY 
 * KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations 
 * under the License.
 *******************************************************************************/
package foundation.ddmore.pharmml08.impl;

import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.validation.Schema;

import foundation.ddmore.pharmml08.IErrorHandler;
import foundation.ddmore.pharmml08.IMarshaller;
import foundation.ddmore.pharmml08.dom.PharmML;

public class MarshallerImpl implements IMarshaller {
	private static final String CONTEXT_NAME = Messages.getString("MarshallerImpl.contextDefn"); //$NON-NLS-1$
	private IErrorHandler errorHandler;
	
	public MarshallerImpl(){
	}
	
	@Override
	public void marshall(PharmML dom, OutputStream os) {
		try {
			JAXBContext context = JAXBContext.newInstance(CONTEXT_NAME);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new DefaultNamespaceMapper());
			m.marshal(dom, os);
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public PharmML unmarshall(InputStream is) {
		try {
			Schema mySchema = PharmMLSchemaFactory.getInstance().createPharmMlSchema();
//			String packageName = PharmML.class.getPackage().getName();
			JAXBContext context = JAXBContext.newInstance(CONTEXT_NAME);
			Unmarshaller u = context.createUnmarshaller();
			u.setEventHandler(new ValidationEventHandler() {
				
				@Override
				public boolean handleEvent(ValidationEvent event) {
					int severity = event.getSeverity();
					switch(severity){
					case ValidationEvent.ERROR:
						errorHandler.handleError(event.getMessage());
						break;
					case ValidationEvent.FATAL_ERROR:
						errorHandler.handleError(event.getMessage());
						break;
					case ValidationEvent.WARNING:
						errorHandler.handleWarning(event.getMessage());
						break;
					}
					return true;
				}
			});
			u.setSchema(mySchema);
			PharmML doc = (PharmML)u.unmarshal(is);
			return doc;
		} catch (JAXBException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	@Override
	public void setErrorHandler(IErrorHandler repFact) {
		this.errorHandler = repFact;
	}

	@Override
	public IErrorHandler getErrorHandler() {
		return this.errorHandler;
	}

}
