/*******************************************************************************
 * Copyright (c) 2013 European Molecular Biology Laboratory,
 * Heidelberg, Germany.
 *
 * Copyright (c) 2017 DDMoRe Foundation,
 * Utrecht, NL.
 *
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in
 * compliance with the License.  You may obtain a copy of
 * the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY 
 * KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations 
 * under the License.
 *******************************************************************************/
package foundation.ddmore.pharmml08.impl;

import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBElement;

import foundation.ddmore.pharmml08.ILibPharmML;
import foundation.ddmore.pharmml08.IMarshaller;
import foundation.ddmore.pharmml08.IPharmMLResource;
import foundation.ddmore.pharmml08.IPharmMLValidator;
import foundation.ddmore.pharmml08.IValidationReport;
import foundation.ddmore.pharmml08.dom.PharmML;
import foundation.ddmore.pharmml08.dom.commontypes.NameType;
import foundation.ddmore.pharmml08.dom.commontypes.SymbolTypeType;
import foundation.ddmore.pharmml08.dom.commontypes.ObjectFactory;
import foundation.ddmore.pharmml08.dom.commontypes.VariableDefinitionType;
import foundation.ddmore.pharmml08.dom.modeldefn.ModelDefinitionType;
import foundation.ddmore.pharmml08.dom.modeldefn.ParameterModelType;
import foundation.ddmore.pharmml08.dom.modeldefn.PopulationParameterType;
import foundation.ddmore.pharmml08.dom.modeldefn.StructuralModelType;

public class LibPharmMLImpl implements ILibPharmML {
	private static final String DEFAULT_NAME = "Stub Model";
	private static final String WRITTEN_VERSION = "0.8.1";
//	private static final String DEFAULT_IND_VAR = "t";
	private static final String DEFAULT_STRUCT_MDL_NAME = "main";
	private IMarshaller marshaller;
	private IPharmMLValidator validator;

	@Override
	public void save(OutputStream opStr, IPharmMLResource resource) {
		// Set the correct written version that we are compliant with.
		resource.getDom().setWrittenVersion(WRITTEN_VERSION);
		this.marshaller.marshall(resource.getDom(), opStr);
	}

	@Override
	public IPharmMLResource createDomFromResource(InputStream inStr) {
		final ValidationReportFactory repFact = new ValidationReportFactory();
		this.marshaller.setErrorHandler(repFact);
		final PharmML dom = this.marshaller.unmarshall(inStr);
		IPharmMLResource retVal = new IPharmMLResource() {
			@Override
			public PharmML getDom() {
				return dom;
			}
			@Override
			public IValidationReport getCreationReport() {
				return repFact.createReport();
			}
		};
		return retVal;
	}

	@Override
	public IPharmMLResource createDom() {
		foundation.ddmore.pharmml08.dom.ObjectFactory fact = new foundation.ddmore.pharmml08.dom.ObjectFactory();
		ObjectFactory commonFact = new ObjectFactory();
		final PharmML dom = fact.createPharmML();
		dom.setWrittenVersion(WRITTEN_VERSION);
		NameType name = commonFact.createNameType();
		name.setValue(DEFAULT_NAME);
		dom.setName(name);
		foundation.ddmore.pharmml08.dom.modeldefn.ObjectFactory mdefnFact = new foundation.ddmore.pharmml08.dom.modeldefn.ObjectFactory();
		ModelDefinitionType mdt = mdefnFact.createModelDefinitionType();
		ParameterModelType pm = mdefnFact.createParameterModelType();
		pm.setBlkId("p1");
		PopulationParameterType spt = mdefnFact.createPopulationParameterType();
		spt.setSymbId("a");
		JAXBElement<PopulationParameterType> param1 = mdefnFact.createPopulationParameter(spt);
		pm.getConditionalStatementAndCommonParameterElementAndAssignStatement().add(param1);
		StructuralModelType structModel = mdefnFact.createStructuralModelType();
		structModel.setBlkId(DEFAULT_STRUCT_MDL_NAME);
		VariableDefinitionType varType = commonFact.createVariableDefinitionType();
		varType.setSymbId("x");
		varType.setSymbolType(SymbolTypeType.REAL);
		JAXBElement<VariableDefinitionType> var1 = commonFact.createVariable(varType);
		structModel.getPopulationParameterAndIndividualParameterAndParameter().add(var1);
		mdt.getParameterModel().add(pm);
		mdt.getStructuralModel().add(structModel);
		mdefnFact.createModelDefinition(mdt);
		dom.setModelDefinition(mdt);
		final ValidationReportFactory repFact = new ValidationReportFactory();
		return new IPharmMLResource() {
			
			@Override
			public PharmML getDom() {
				return dom;
			}
			
			@Override
			public IValidationReport getCreationReport() {
				return repFact.createReport();
			}
		};
	}

	@Override
	public void setValidator(IPharmMLValidator validator) {
		this.validator = validator;
	}

	@Override
	public IPharmMLValidator getValidator() {
		return this.validator;
	}

	@Override
	public void setMarshaller(IMarshaller marshaller) {
		this.marshaller = marshaller;
	}

	@Override
	public IMarshaller getMarshaller() {
		return this.marshaller;
	}

}
