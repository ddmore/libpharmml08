/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.commontypes.BooleanType
import foundation.ddmore.pharmml08.dom.commontypes.IntValueType
import foundation.ddmore.pharmml08.dom.commontypes.MatrixRowType
import foundation.ddmore.pharmml08.dom.commontypes.MatrixType
import foundation.ddmore.pharmml08.dom.commontypes.MatrixTypeType
import foundation.ddmore.pharmml08.dom.commontypes.MinusInfType
import foundation.ddmore.pharmml08.dom.commontypes.PlusInfType
import foundation.ddmore.pharmml08.dom.commontypes.RealValueType
import foundation.ddmore.pharmml08.dom.commontypes.RhsType
import foundation.ddmore.pharmml08.dom.commontypes.StringValueType
import foundation.ddmore.pharmml08.dom.commontypes.SymbolRefType
import foundation.ddmore.pharmml08.dom.commontypes.VectorSelectorType
import foundation.ddmore.pharmml08.dom.commontypes.VectorType
import foundation.ddmore.pharmml08.dom.dataset.ColumnRefType
import foundation.ddmore.pharmml08.dom.maths.BinaryOperator
import foundation.ddmore.pharmml08.dom.maths.BinopType
import foundation.ddmore.pharmml08.dom.maths.ConditionType
import foundation.ddmore.pharmml08.dom.maths.ConstantType
import foundation.ddmore.pharmml08.dom.maths.ConstantValueType
import foundation.ddmore.pharmml08.dom.maths.FunctionCallType
import foundation.ddmore.pharmml08.dom.maths.LogicBinOpType
import foundation.ddmore.pharmml08.dom.maths.LogicUniOpType
import foundation.ddmore.pharmml08.dom.maths.LogicalBinOperator
import foundation.ddmore.pharmml08.dom.maths.LogicalUniOperator
import foundation.ddmore.pharmml08.dom.maths.MatrixUniOpType
import foundation.ddmore.pharmml08.dom.maths.MatrixUniOperatorType
import foundation.ddmore.pharmml08.dom.maths.OtherwiseType
import foundation.ddmore.pharmml08.dom.maths.PieceType
import foundation.ddmore.pharmml08.dom.maths.PiecewiseType
import foundation.ddmore.pharmml08.dom.maths.UnaryOperator
import foundation.ddmore.pharmml08.dom.maths.UniopType
import foundation.ddmore.pharmml08.dom.modeldefn.DistributionType
import foundation.ddmore.pharmml08.dom.modeldefn.UncertMLType
import foundation.ddmore.pharmml08.dom.probonto.ProbOntoType
import foundation.ddmore.pharmml08.dom.commontypes.ObjectFactory
import groovy.transform.TypeChecked

import javax.xml.bind.JAXB

import static foundation.ddmore.pharmml08.builder.FactoryFactory.*

import javax.xml.bind.JAXBElement

import static FactoryFactory.*

/**
 * Created by stumoodie on 04/01/2017.
 */
@TypeChecked
class MathsBuilder {

    static JAXBElement<BinopType> createBinop(BinaryOperator op, JAXBElement<?> op1, JAXBElement<?> op2){
        def retVal = newMathsFactory().createBinopType()
        retVal.op = op
        retVal.getExpression().add(op1)
        retVal.getExpression().add(op2)
        newMathsFactory().createBinop(retVal)
    }

    static JAXBElement<UniopType> createUniop(UnaryOperator op, JAXBElement<?> opnd){
        def of = newMathsFactory()
        def retVal = of.createUniopType()
        retVal.setExpression(opnd)
        retVal.op = op
        of.createUniop(retVal)
    }

    static JAXBElement<IntValueType> createInt(Integer value){
        def of = new ObjectFactory()
        def retVal = of.createIntValueType()
        retVal.value = value
        of.createInt(retVal)
    }

    static JAXBElement<StringValueType> createString(String value){
        def retVal = newCTFactory().createStringValueType()
        retVal.value = value

        newCTFactory().createString(retVal)
    }


    static JAXBElement<RealValueType> createReal(Double value){
        def of = new ObjectFactory()
        def retVal = of.createRealValueType()
        retVal.value = value
        of.createReal(retVal)
    }

    static RhsType createRhs(JAXBElement<?> expr){
        def of = new ObjectFactory()
        RhsType retVal = of.createRhsType()
        retVal.expression = expr
        retVal
    }

    static JAXBElement<RhsType> createAssign(JAXBElement<?> expr){
        def rhs = createRhs(expr)
        newCTFactory().createAssign(rhs)
    }

    static JAXBElement<RhsType> createAssign(RhsType rhs){
        newCTFactory().createAssign(rhs)
    }

    static JAXBElement<PiecewiseType> createPiecewise(PieceType ... pieces){
        def of = newMathsFactory()
        def retVal = of.createPiecewiseType()
        pieces.each { p->
            retVal.getPiece().add((PieceType)p)
        }
        of.createPiecewise(retVal)
    }

    static PieceType createPiece(ConditionType cond, JAXBElement<?> expr){
        def of = newMathsFactory()
        def piece = of.createPieceType()
        piece.setCondition(cond)
        piece.setExpression(expr)
        piece
    }

    static ConditionType createCondition(JAXBElement<?> logicExpr) {
        def of = newMathsFactory()
        def retVal = of.createConditionType()
        retVal.setExpression(logicExpr)
        retVal
    }

    static JAXBElement<OtherwiseType> createOtherwise(){
        def of = newMathsFactory()
        def retVal = of.createOtherwiseType()
        of.createOtherwise(retVal)
    }

    static JAXBElement<? extends BooleanType> createBoolean(boolean isTrue){
        def of = newCTFactory()
        JAXBElement<? extends BooleanType> retVal = null
        if(isTrue){
            retVal = of.createTrue(of.createTrueBooleanType())

        }
        else {
            retVal = of.createFalse(of.createFalseBooleanType())
        }
        retVal
    }

    static JAXBElement<LogicUniOpType> createLogicUniop(LogicalUniOperator operator, JAXBElement<?> expr){
        def of = newMathsFactory()
        def retVal = of.createLogicUniOpType()
        retVal.op = operator
        retVal.expression = expr
        of.createLogicUniop(retVal)
    }

    static JAXBElement<LogicBinOpType> createLogicBinop(LogicalBinOperator operator, JAXBElement<?> operand1, JAXBElement<?> operand2){
        def of = newMathsFactory()
        def retVal = of.createLogicBinOpType()
        retVal.op = operator
        retVal.getExpression().add(operand1)
        retVal.getExpression().add(operand2)
        of.createLogicBinop(retVal)
    }


    static JAXBElement<MatrixUniOpType> createMatrixUniOp(MatrixUniOperatorType operator, JAXBElement<?> operand){
        def of = newMathsFactory()
        def retVal = of.createMatrixUniOpType()

        retVal.op = operator
        retVal.expression = operand

        of.createMatrixUniop(retVal)
    }


    static JAXBElement<ConstantType> createConstant(ConstantValueType val){
        def of = newMathsFactory()
        def retVal = of.createConstantType()
        retVal.setOp(val)
        of.createConstant(retVal)
    }

    static JAXBElement<PlusInfType> createPlusInf(){
        newCTFactory().createPlusInf(newCTFactory().createPlusInfType())
    }

    static JAXBElement<MinusInfType> createMinusInf(){
        newCTFactory()createMinusInf(newCTFactory().createMinusInfType())
    }


    static SymbolRefType createSymbRefType(String blkId, String symbId){
        def ref = newCTFactory().createSymbolRefType()
        ref.blkIdRef = blkId
        ref.symbIdRef = symbId
        ref
    }

    static SymbolRefType createSymbRefType(String symbId){
        createSymbRefType(null, symbId)
    }

    static JAXBElement<SymbolRefType> createSymbRef(String blkId, String symbId){
        newCTFactory().createSymbRef(createSymbRefType(blkId, symbId))
    }

    static JAXBElement<SymbolRefType> createSymbRef(String symbId){
        newCTFactory().createSymbRef(createSymbRefType(symbId))
    }

    static JAXBElement<ColumnRefType> createColumnRef(String colRef){
        newDatasetFactory().createColumnRef(createColumnRefType(colRef))
    }

    static ColumnRefType createColumnRefType(String colIdRef){
        def retVal = newDatasetFactory().createColumnRefType()

        retVal.columnIdRef = colIdRef

        retVal
    }



    static FunctionCallType createFunctionCallType(SymbolRefType symbRef, FunctionCallType.FunctionArgument ... args){
        def retVal = newMathsFactory().createFunctionCallType()
        retVal.setSymbRef(symbRef)
        args.each {a->
            retVal.getFunctionArgument().add((FunctionCallType.FunctionArgument)a)
        }

        retVal
    }

    static JAXBElement<FunctionCallType> createFunctionCall(SymbolRefType symbRef, FunctionCallType.FunctionArgument ... args){
        newMathsFactory().createFunctionCall(createFunctionCallType(symbRef, args))
    }


    static FunctionCallType.FunctionArgument createFunctionArgument(String argName, RhsType rhs){
        def retVal = newMathsFactory().createFunctionCallTypeFunctionArgument()
        retVal.symbId = argName
        retVal.assign = rhs

        retVal
    }

    static FunctionCallType.FunctionArgument createFunctionArgument(String argName, JAXBElement<?> expr){
        def retVal = newMathsFactory().createFunctionCallTypeFunctionArgument()
        retVal.symbId = argName
        retVal.expression = expr

        retVal
    }


    static DistributionType createDistribution(UncertMLType node){
        def of = new foundation.ddmore.pharmml08.dom.modeldefn.ObjectFactory()
        def retVal = of.createDistributionType()
        retVal.setUncertML(node)
        retVal
    }

    static DistributionType createDistribution(ProbOntoType node){
        def of = new foundation.ddmore.pharmml08.dom.modeldefn.ObjectFactory()
        def retVal = of.createDistributionType()
        retVal.setProbOnto(node)
        retVal
    }


    static JAXBElement<VectorType> createVector(JAXBElement<?> ... vectElements){
        def of = new ObjectFactory()
        def retVal = of.createVectorType()
        def vels = of.createVectorElementsType()
        retVal.setVectorElements(vels)
        vectElements.each {ve->
            vels.getExpressionOrAssign().add((JAXBElement<?>)ve)
        }

        of.createVector(retVal)
    }


    static JAXBElement<VectorSelectorType> createVectorSelector(SymbolRefType vectRef, RhsType ... selectIndexes){
        def of = new ObjectFactory()
        def retVal = of.createVectorSelectorType()
        retVal.setSymbRef(vectRef)
        selectIndexes.each {si->
            def vc = of.createMatrixVectorIndexType()
            vc.setAssign((RhsType)si)
            retVal.getCellOrSegment().add(vc)
        }

        of.createVectorSelector(retVal)
    }

    static JAXBElement<MatrixType> createMatrix(MatrixTypeType matType, MatrixRowType ... rows){
        def retVal = newCTFactory().createMatrixType()

        retVal.matrixType = matType

        rows.each {r->
            retVal.getMatrixColumnOrMatrixRowOrMatrixCell().add((MatrixRowType)r)
        }

        newCTFactory().createMatrix(retVal)
    }

    static MatrixRowType createMatrixRow(JAXBElement<?> ... exprs){
        def retVal = newCTFactory().createMatrixRowType()

        exprs.each {e->
            retVal.getExpressionOrAssign().add((JAXBElement<?>)e)
        }

        retVal
    }

}
