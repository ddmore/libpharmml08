/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.commontypes.RhsType
import foundation.ddmore.pharmml08.dom.commontypes.SymbolRefType
import foundation.ddmore.pharmml08.dom.commontypes.VariableAssignmentType
import foundation.ddmore.pharmml08.dom.modellingsteps.AlgorithmType
import foundation.ddmore.pharmml08.dom.modellingsteps.CommonModellingStepType
import foundation.ddmore.pharmml08.dom.modellingsteps.EstimationOperationType
import foundation.ddmore.pharmml08.dom.modellingsteps.EstimationStepType
import foundation.ddmore.pharmml08.dom.modellingsteps.ModellingStepsType
import foundation.ddmore.pharmml08.dom.modellingsteps.OperationPropertyType
import foundation.ddmore.pharmml08.dom.modellingsteps.ParameterEstimateType
import foundation.ddmore.pharmml08.dom.modellingsteps.SimulationOperationType
import foundation.ddmore.pharmml08.dom.modellingsteps.SimulationStepType
import foundation.ddmore.pharmml08.dom.modellingsteps.StepDependencyType
import foundation.ddmore.pharmml08.dom.modellingsteps.StepType
import groovy.transform.TypeChecked

import javax.xml.bind.JAXBContext
import javax.xml.bind.JAXBElement

import static foundation.ddmore.pharmml08.builder.FactoryFactory.newCTFactory
import static foundation.ddmore.pharmml08.builder.FactoryFactory.newModStepsFactory

@TypeChecked
class ModellingStepsBuilder {

    static ModellingStepsType createModellingSteps(StepDependencyType deps, JAXBElement<CommonModellingStepType> ... cms){
        def retVal = newModStepsFactory().createModellingStepsType()

        cms.each {ms->
            retVal.getCommonModellingStep().add((JAXBElement<CommonModellingStepType>)ms)
        }
        retVal.stepDependencies = deps

        retVal
    }


    static StepDependencyType createStepDependencies(StepType ... steps){
        def retVal = newModStepsFactory().createStepDependencyType()

        steps.each {s->
            retVal.getStep().add((StepType)s)
        }

        retVal
    }


    static StepType createStep(String oidRef, String ... depOidRefs){
        def retVal = newModStepsFactory().createStepType()

        retVal.oidRef = newCTFactory().createOidRefType()
        retVal.oidRef.oidRef = oidRef

        def dt = newModStepsFactory().createDependentsType()
        depOidRefs.each {dep->
            def dtRef = newCTFactory().createOidRefType()
            dtRef.setOidRef((String)dep)
            dt.getOidRef().add(dtRef)
        }
        // can have multiple dependents, but we only need to define it once
        retVal.getDependents().add(dt)

        retVal
    }


    static JAXBElement<EstimationStepType> createEstimationStep(String oid, String datasetRef, List<ParameterEstimateType> paramsToEstimate, List<EstimationOperationType> estOps){
        def retVal = newModStepsFactory().createEstimationStepType()

        retVal.oid = oid
        def te = newModStepsFactory().createToEstimateType()
        retVal.parametersToEstimate = te
        paramsToEstimate.each {pte->
            te.getParameterEstimation().add(pte)
        }

        estOps.each {eo->
            retVal.getOperation().add(eo)
        }

        retVal.externalDataSetReference = newModStepsFactory().createExternalDataSetReferenceType()
        retVal.externalDataSetReference.oidRef = newCTFactory().createOidRefType()
        retVal.externalDataSetReference.oidRef.oidRef = datasetRef

        newModStepsFactory().createEstimationStep(retVal)
    }


    static ParameterEstimateType createParameterEstimate(SymbolRefType symbRef, JAXBElement<?> initEst, boolean isFixed = false, RhsType lower = null, RhsType upper = null){
        def retVal = newModStepsFactory().createParameterEstimateType()

        retVal.symbRef = symbRef
        retVal.initialEstimate = newModStepsFactory().createInitialEstimateType()
        retVal.initialEstimate.setExpression(initEst)
        retVal.initialEstimate.fixed = isFixed

        if(lower)
            retVal.lowerBound = lower

        if(upper)
            retVal.upperBound = upper

        retVal
    }

    /**
     * create an estimation operation
     * @param opType
     * @param order
     * @param name
     * @param algType
     * @param opProps
     * @return
     * @Deprecated use createEstOperation instead
     */
    @Deprecated
    static EstimationOperationType createOperation(String opType, int order, String name = null, AlgorithmType algType = null, List<OperationPropertyType> opProps){
        createEstOperation(opType, order, name, algType, opProps)
    }

    static EstimationOperationType createEstOperation(String opType, int order, String name = null, AlgorithmType algType = null, List<OperationPropertyType> opProps){
        def retVal = newModStepsFactory().createEstimationOperationType()

        retVal.opType = opType
        retVal.name = newCTFactory().createNameType()
        retVal.name.value = name
        retVal.order = order
        retVal.algorithm = algType
        opProps.each {p->
            retVal.getProperty().add(p)
        }

        retVal
    }


    static AlgorithmType createAlgorithm(String name, String defn = null, List<OperationPropertyType> algProps){
        def retVal = newModStepsFactory().createAlgorithmType()

        retVal.name = newCTFactory().createNameType()
        retVal.name.value = name
        retVal.definition = defn
        algProps.each {p->
            retVal.getProperty().add(p)
        }

        retVal
    }


    static OperationPropertyType createProperty(String propName, RhsType value, String descn = null){
        def retVal = newModStepsFactory().createOperationPropertyType()

        retVal.name = propName
        retVal.assign = value
        def at = newCTFactory().createAnnotationType()
        at.value = descn
        retVal.description = at

        retVal
    }

    static JAXBElement<SimulationStepType> createSimulationStep(String oid, String datasetRef, List<VariableAssignmentType> varAssignments, List<SimulationOperationType> estOps){
        def retVal = newModStepsFactory().createSimulationStepType()

        retVal.oid = oid
        varAssignments.each {pte->
            retVal.getVariableAssignment().add(pte)
        }

        estOps.each {eo->
            retVal.getOperation().add(eo)
        }

        retVal.externalDataSetReference = newModStepsFactory().createExternalDataSetReferenceType()
        retVal.externalDataSetReference.oidRef = newCTFactory().createOidRefType()
        retVal.externalDataSetReference.oidRef.oidRef = datasetRef

        newModStepsFactory().createSimulationStep(retVal)
    }

    static SimulationOperationType createSimOperation(String opType, int order, String name = null, AlgorithmType algType = null, List<OperationPropertyType> opProps){
        def retVal = newModStepsFactory().createSimulationOperationType()

        retVal.opType = opType
        retVal.name = newCTFactory().createNameType()
        retVal.name.value = name
        retVal.order = order
        retVal.algorithm = algType
        opProps.each {p->
            retVal.getProperty().add(p)
        }

        retVal
    }

    static VariableAssignmentType createVariableAssignment(SymbolRefType symbRef, RhsType rhsType){
        def retVal = newCTFactory().createVariableAssignmentType()

        retVal.symbRef = symbRef
        retVal.assign = rhsType

        retVal
    }


}
