/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.commontypes.PharmMLRootType
import foundation.ddmore.pharmml08.dom.commontypes.SymbolRefType
import foundation.ddmore.pharmml08.dom.commontypes.SymbolTypeType
import foundation.ddmore.pharmml08.dom.dataset.CategoryMappingType
import foundation.ddmore.pharmml08.dom.dataset.ColumnDefnType
import foundation.ddmore.pharmml08.dom.dataset.ColumnMappingType
import foundation.ddmore.pharmml08.dom.dataset.ColumnRefType
import foundation.ddmore.pharmml08.dom.dataset.ColumnTransformationType
import foundation.ddmore.pharmml08.dom.dataset.ColumnTypeType
import foundation.ddmore.pharmml08.dom.dataset.DataSetType
import foundation.ddmore.pharmml08.dom.dataset.ExternalFileType
import foundation.ddmore.pharmml08.dom.dataset.MapType
import foundation.ddmore.pharmml08.dom.dataset.TargetMappingType
import foundation.ddmore.pharmml08.dom.maths.PiecewiseType
import foundation.ddmore.pharmml08.dom.trialdesign.ExternalDataSetType
import foundation.ddmore.pharmml08.dom.trialdesign.MultipleDVMappingType

import static foundation.ddmore.pharmml08.builder.FactoryFactory.*

/**
 * Created by stumoodie on 12/04/2017.
 */
class TrialDesignBuilder {

    def static createTrialDesign(ExternalDataSetType ... datasets){
        def retVal = newDesignFactory().createTrialDesignType()

        datasets.each {eds->
            retVal.getExternalDataSet().add(eds)
        }

        retVal
    }

    def static createExternalDataset(String oid, String toolName, DataSetType ds, PharmMLRootType ... columnMappings){
        def retVal = newDesignFactory().createExternalDataSetType()

        retVal.oid = oid
        retVal.toolName = toolName

        columnMappings.each {cm->
            assert cm instanceof ColumnMappingType || cm instanceof MultipleDVMappingType || cm instanceof ColumnTransformationType
            retVal.getColumnMappingOrColumnTransformationOrMultipleDVMapping().add(cm)
        }
        retVal.dataSet = ds

        retVal
    }


    def static createDataset(ExternalFileType extnlFile, ColumnDefnType ... colDefns){
        def retVal = newDatasetFactory().createDataSetType()

        retVal.definition = newDatasetFactory().createHeaderColumnsDefinitionType()
        colDefns.each {cd->
            retVal.definition.getColumn().add(cd)
        }

        retVal.externalFile = extnlFile

        retVal
    }


    def static createExternalFileType(String path){
        def retVal = newDatasetFactory().createExternalFileType()

        retVal.path = path
        retVal.format = "CSV"
        retVal.delimiter = "COMMA"

        retVal
    }


    def static createColumnDefinition(String colId, int colNum, ColumnTypeType colType, SymbolTypeType valType){
        def retVal = newDatasetFactory().createColumnDefnType()

        retVal.columnId = colId
        retVal.columnNum = colNum
        retVal.getColumnType().add(colType)
        retVal.valueType = valType

        retVal
    }


    def static createColumnMapping(ColumnRefType colRef, SymbolRefType symbRef){
        def retVal = newDatasetFactory().createColumnMappingType()

        retVal.columnRef = colRef
        retVal.symbRef = symbRef

        retVal
    }

    def static createColumnMapping(ColumnRefType colRef, TargetMappingType ... tgtMappings){
        def retVal = newDatasetFactory().createColumnMappingType()

        retVal.columnRef = colRef

        tgtMappings.each {tm->
            retVal.targetMapping.add(tm)
        }

        retVal
    }

    def static createTargetMapping(String blkIdRef, List<Map> targets){
        def retVal = newDatasetFactory().createTargetMappingType()

        retVal.blkIdRef = blkIdRef
        targets.each {t->
            def tm = newDatasetFactory().createMapType()
            tm.admNumber = t.adm
            tm.dataSymbol = t.dataSymbol
            retVal.getMap().add(tm)
        }

        retVal
    }

    def static createTargetMapping(String blkIdRef, int adm){
        def retVal = newDatasetFactory().createTargetMappingType()

        retVal.blkIdRef = blkIdRef
        def tm = newDatasetFactory().createMapType()
        tm.admNumber = adm
        retVal.getMap().add(tm)

        retVal
    }


    def static createColumnMapping(ColumnRefType colRef, PiecewiseType pw){
        def retVal = newDatasetFactory().createColumnMappingType()

        retVal.columnRef = colRef
        retVal.piecewise = pw

        retVal
    }

    def private static createMappings(ColumnMappingType cmt, List<Map> catMappings){
        def cm = newDatasetFactory().createCategoryMappingType()
        cmt.getCategoryMapping().add(cm)

        catMappings.each {catMap->
            def mapType = newDatasetFactory().createMapType()
            mapType.modelSymbol = catMap.modelSymb
            mapType.dataSymbol = catMap.dataSymb
            cm.getMap().add(mapType)
        }

        cmt
    }

    def static createColumnMapping(ColumnRefType colRef, SymbolRefType symbRef, List<Map> catMappings){
        def retVal = createColumnMapping(colRef, symbRef)

        createMappings(retVal, catMappings)
    }

    def static createColumnMapping(ColumnRefType colRef, PiecewiseType pwt, List catMappings){
        def retVal = createColumnMapping(colRef, pwt)

        createColumnMapping(retVal, catMappings)
    }


    def static createMultiDvMapping(ColumnRefType colRef, PiecewiseType pw){
        def retVal = newDesignFactory().createMultipleDVMappingType()

        retVal.columnRef = colRef
        retVal.piecewise = pw

        retVal
    }

}
