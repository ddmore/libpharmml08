/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import eu.ddmore.libpharmml08.dom.modeldefn.*
import foundation.ddmore.pharmml08.dom.commontypes.LevelReferenceType
import foundation.ddmore.pharmml08.dom.commontypes.OrderedAttributeType
import foundation.ddmore.pharmml08.dom.commontypes.PMFtransformType
import foundation.ddmore.pharmml08.dom.commontypes.RhsType
import foundation.ddmore.pharmml08.dom.commontypes.SymbolRefType
import foundation.ddmore.pharmml08.dom.modeldefn.*

import javax.xml.bind.JAXBElement

import static foundation.ddmore.pharmml08.builder.FactoryFactory.newCTFactory
import static foundation.ddmore.pharmml08.builder.FactoryFactory.newMDefnFactory
import static foundation.ddmore.pharmml08.builder.MathsBuilder.createSymbRefType

/**
 * Created by stumoodie on 04/01/2017.
 */
class ObservationModelBuilder {

    static ObservationModelType createObservationModel(String blkId, ContinuousObservationModelType com){
        def retVal = newMDefnFactory().createObservationModelType()
        retVal.blkId = blkId
        retVal.continuousData = com

        retVal
    }

    static ObservationModelType createObservationModel(String blkId,  DiscreteType com){
        def retVal = newMDefnFactory().createObservationModelType()
        retVal.blkId = blkId
        retVal.discrete = com

        retVal
    }

    static def createContinuousObservation(JAXBElement<? extends ObservationErrorType> obsErr, JAXBElement<?> ... paramDefns){
        def retVal = newMDefnFactory().createContinuousObservationModelType()
        retVal.observationError = obsErr
        paramDefns.each {p->
            retVal.getCommonParameterElementAndAssignStatementAndConditionalStatement().add(p)
        }

        retVal
    }

    static def createStructuredObs(String symbId, SymbolRefType outputVar, RhsType errFunc, SymbolRefType residualErr, TransformationType trans = null){
        def retVal = newMDefnFactory().createStructuredObsError()
        retVal.symbId = symbId
        retVal.output = newMDefnFactory().createStructuredObsErrorOutput()
        retVal.output.symbRef = outputVar
        retVal.errorModel = newMDefnFactory().createStructuredObsErrorErrorModel()
        retVal.errorModel.assign = errFunc
        retVal.residualError = newMDefnFactory().createStructuredObsErrorResidualError()
        retVal.residualError.symbRef = residualErr
        if(trans){
            retVal.transformation = newMDefnFactory().createLRHSTransformationType()
            retVal.transformation.type = trans
        }

        newMDefnFactory().createObservationError(retVal)
    }


    static def createGeneralObs(String symbId, RhsType rhs, TransformationType trans = null){
        def retVal = newMDefnFactory().createGeneralObsError()
        retVal.symbId = symbId
        retVal.assign = rhs
        if(trans){
            retVal.LHSTransformation = newMDefnFactory().createLHSTransformationType()
            retVal.LHSTransformation.type = trans
        }

        newMDefnFactory().createObservationError(retVal)
    }


    static def createDiscreteObservation(CommonObservationModelType com){
        def retVal = newMDefnFactory().createDiscreteType()
        switch(com){
            case TimeToEventDataType:
                retVal.timeToEventData = com
                break;
            case CategoricalDataType:
                retVal.categoricalData = com
                break;
            case CountDataType:
                retVal.countData = com
                break;
        }

        retVal
    }

    static def createTTEObs(String eventVar, String hazVar, RhsType hazFunc){
        def retVal = newMDefnFactory().createTimeToEventDataType()
        retVal.eventVariable = newMDefnFactory().createCommonDiscreteVariableType()
        retVal.eventVariable.symbId = eventVar
        def tte = newMDefnFactory().createTTEFunctionType()
        tte.symbId = hazVar
        tte.assign = hazFunc
        retVal.getHazardFunction().add(tte)

        retVal
    }

    static def createCategoricalObs(String categoryVariable, DistributionType pmfDist, String ... categories){
        def retVal = newMDefnFactory().createCategoricalDataType()
        retVal.ordered = OrderedAttributeType.NO
        categories.each {c->
            def cat = newMDefnFactory().createCommonDiscreteVariableType()
            cat.symbId = c
            def loc = newMDefnFactory().createListOfCategoriesType()
            loc.getCategory().add(cat)
            retVal.setListOfCategories(loc)
        }
        retVal.categoryVariable = newMDefnFactory().createCommonDiscreteVariableType()
        retVal.categoryVariable.symbId = categoryVariable
        def pmf = newMDefnFactory().createCategoricalPMFType()
        pmf.distribution = pmfDist
        retVal.getPMF().add(pmf)

        retVal
    }

    static def createCountObs(String countVariable, DistributionType pmfDist){
        def retVal = newMDefnFactory().createCountDataType()
        retVal.countVariable = newMDefnFactory().createCommonDiscreteVariableType()
        retVal.countVariable.symbId = countVariable
        def pmf = newMDefnFactory().createCountPMFType()
        pmf.transform = PMFtransformType.IDENTITY
        pmf.distribution = pmfDist
        retVal.getPMF().add(pmf)

        retVal
    }
}
