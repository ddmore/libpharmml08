/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.commontypes.RhsType
import eu.ddmore.libpharmml08.dom.probonto.*
import foundation.ddmore.pharmml08.dom.probonto.DistributionParameterType
import foundation.ddmore.pharmml08.dom.probonto.DistroNameType
import foundation.ddmore.pharmml08.dom.probonto.DistroType
import foundation.ddmore.pharmml08.dom.probonto.ObjectFactory
import foundation.ddmore.pharmml08.dom.probonto.ParameterNameType
import foundation.ddmore.pharmml08.dom.probonto.ProbOntoType
import groovy.transform.TypeChecked

/**
 * Created by stumoodie on 04/01/2017.
 */
@TypeChecked
class ProbontoBuilder {

    static ProbOntoType createProbonto(DistroNameType name, DistroType type, DistributionParameterType ... params){
        def of = new ObjectFactory()
        def retVal = of.createProbOntoType()
        retVal.name = name
        retVal.type = type
        params.each { retVal.getParameter().add((DistributionParameterType)it) }

        retVal
    }


    static DistributionParameterType createDistnParameter(ParameterNameType name, RhsType assign){
        def of = new ObjectFactory()
        def retVal = of.createDistributionParameterType()
        retVal.name = name
        retVal.assign = assign
        retVal
    }

}
