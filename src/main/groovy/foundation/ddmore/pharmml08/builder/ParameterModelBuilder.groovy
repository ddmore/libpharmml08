/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.commontypes.LevelReferenceType
import foundation.ddmore.pharmml08.dom.commontypes.RhsType
import foundation.ddmore.pharmml08.dom.commontypes.SymbolRefType
import eu.ddmore.libpharmml08.dom.modeldefn.*
import foundation.ddmore.pharmml08.dom.modeldefn.CategoricalRelationType
import foundation.ddmore.pharmml08.dom.modeldefn.CorrelationType
import foundation.ddmore.pharmml08.dom.modeldefn.CovariateRelationType
import foundation.ddmore.pharmml08.dom.modeldefn.DistributionType
import foundation.ddmore.pharmml08.dom.modeldefn.FixedEffectRelationType
import foundation.ddmore.pharmml08.dom.modeldefn.IndividualParameterType
import foundation.ddmore.pharmml08.dom.modeldefn.LHSTransformationType
import foundation.ddmore.pharmml08.dom.modeldefn.LRHSTransformationType
import foundation.ddmore.pharmml08.dom.modeldefn.ObjectFactory
import foundation.ddmore.pharmml08.dom.modeldefn.ParameterModelType
import foundation.ddmore.pharmml08.dom.modeldefn.ParameterRandomEffectType
import foundation.ddmore.pharmml08.dom.modeldefn.ParameterRandomVariableType
import foundation.ddmore.pharmml08.dom.modeldefn.PopulationParameterType
import foundation.ddmore.pharmml08.dom.modeldefn.SimpleParameterType
import foundation.ddmore.pharmml08.dom.modeldefn.StructuredModelType
import foundation.ddmore.pharmml08.dom.modeldefn.TransformationType
import groovy.transform.TypeChecked

import javax.xml.bind.JAXBElement

import static FactoryFactory.*
import static MathsBuilder.*

/**
 * Created by stumoodie on 04/01/2017.
 */
@TypeChecked
class ParameterModelBuilder {

    static ParameterModelType createParameterModel(String blkId, JAXBElement<?>... defns){
        def retVal = newMDefnFactory().createParameterModelType()
        retVal.blkId = blkId
        for(def defn : defns){
            retVal.getConditionalStatementAndCommonParameterElementAndAssignStatement().add(defn)
        }

        retVal
    }

    static JAXBElement<PopulationParameterType> createPopulationParam(String symbId, RhsType rhs = null){
        createPopulationParam(symbId, rhs, null, null)
    }

    static JAXBElement<PopulationParameterType> createPopulationParam(String symbId, DistributionType distn, LevelReferenceType levelRef){
        createPopulationParam(symbId, null, distn, levelRef)
    }

    static JAXBElement<PopulationParameterType> createPopulationParam(String symbId, RhsType rhs, DistributionType distn, LevelReferenceType levelRef){
        def retVal = newMDefnFactory().createPopulationParameterType()
        retVal.symbId = symbId
        if(rhs){
            retVal.assign = rhs
        }
        else if(distn){
            retVal.distribution = distn
        }
        if(levelRef) {
            retVal.getVariabilityReference().add(levelRef)
        }

        newMDefnFactory().createPopulationParameter(retVal)
    }

    static JAXBElement<SimpleParameterType> createSimpleParam(String symbId, RhsType rhs=null){
        def retVal = newMDefnFactory().createSimpleParameterType()
        retVal.symbId = symbId
        if(rhs){
            retVal.assign = rhs
        }

        newMDefnFactory().createSimpleParameter(retVal)
    }

    static JAXBElement<ParameterRandomVariableType> createRandomVariable(String symbId, DistributionType dist, List<LevelReferenceType> varLevel, LHSTransformationType lhsTrans = null){
        def retVal = newMDefnFactory().createParameterRandomVariableType()
        retVal.symbId = symbId
        retVal.distribution = dist
        retVal.LHSTransformation = lhsTrans
        retVal.getVariabilityReference().addAll(varLevel)

        newMDefnFactory().createRandomVariable(retVal)
    }

    static LHSTransformationType createRVTransform(TransformationType type){
        def retVal = newMDefnFactory().createLHSTransformationType()
        retVal.type = type

        retVal
    }


    static LevelReferenceType createVariabilityRef(String varBlkId, String levelName){

        def retVal = newCTFactory().createLevelReferenceType()
        retVal.symbRef = createSymbRefType(varBlkId, levelName)
        retVal
    }


    static JAXBElement<IndividualParameterType> createIndividualParameter(String symbId, RhsType assignedVal){
        def retVal = newMDefnFactory().createIndividualParameterType()
        retVal.symbId = symbId
        retVal.assign = assignedVal

        newMDefnFactory().createIndividualParameter(retVal)
    }

    static JAXBElement<IndividualParameterType> createStructuredIndividualParameter(String symbId, StructuredModelType sm){
        def retVal = newMDefnFactory().createIndividualParameterType()
        retVal.symbId = symbId

        retVal.structuredModel = sm
        newMDefnFactory().createIndividualParameter(retVal)
    }

    static StructuredModelType createStructuredModel(StructuredModelType.LinearCovariate lc, List<SymbolRefType> randEffs, LRHSTransformationType transform = null){
        def retVal = newMDefnFactory().createStructuredModelType()
        retVal.linearCovariate = lc

        randEffs.each {re->
            def pre = newMDefnFactory().createParameterRandomEffectType()
            pre.setSymbRef(re)
            retVal.getRandomEffects().add(pre)
        }

        retVal.transformation = transform
        retVal
    }

    static StructuredModelType createStructuredModel(RhsType rhsExpr){
        def retVal = newMDefnFactory().createStructuredModelType()
        def gc = new StructuredModelType.GeneralCovariate()
        gc.assign = rhsExpr
        retVal.generalCovariate = gc

        retVal
    }

    static StructuredModelType createStructuredModel(StructuredModelType.GeneralCovariate gc, List<SymbolRefType> randEffs, LRHSTransformationType transform = null){
        def retVal = newMDefnFactory().createStructuredModelType()
        retVal.generalCovariate= gc

        randEffs.each {re->
            def pre = newMDefnFactory().createParameterRandomEffectType()
            pre.setSymbRef(re)
            retVal.getRandomEffects().add(pre)
        }

        retVal.transformation = transform
        retVal
    }

    static LRHSTransformationType createParameterTransform(TransformationType tt){
        def retVal = new LRHSTransformationType()
        retVal.type = tt

        retVal
    }


    static StructuredModelType.LinearCovariate createLinearCovariate(RhsType pvAssign, List<CovariateRelationType> crs){
        def retVal = new StructuredModelType.LinearCovariate()
        def pv = new StructuredModelType.LinearCovariate.PopulationValue()
        pv.assign = pvAssign
        retVal.populationValue = pv
        retVal.getCovariate().addAll(crs)
        retVal
    }


    static StructuredModelType.GeneralCovariate createGeneralCovariate(RhsType rhs){
        def retVal = new StructuredModelType.GeneralCovariate()
        retVal.assign = rhs
        retVal
    }


    static CovariateRelationType createCovariateRelation(SymbolRefType cov, FixedEffectRelationType fer){
        def retVal = new CovariateRelationType()
        retVal.getFixedEffect().add(fer)
        retVal.symbRef = cov

        retVal
    }

    static FixedEffectRelationType createFixedEffectRelationCategorical(SymbolRefType sr, String catId){
        def retVal = newMDefnFactory().createFixedEffectRelationType()
        retVal.setExpression(newCTFactory().createSymbRef(sr))
        def src = new CategoricalRelationType()
        src.catId = catId
        retVal.category = src

        retVal
    }

    static FixedEffectRelationType createFixedEffectRelationCategorical(JAXBElement<?> sr, String catId){
        def retVal = ObjectFactory.newInstance().createFixedEffectRelationType()
        retVal.setExpression(sr)
        def src = new CategoricalRelationType()
        src.catId = catId
        retVal.category = src

        retVal
    }

    static FixedEffectRelationType createFixedEffectRelationParamRef(SymbolRefType sr){
        def retVal = newMDefnFactory().createFixedEffectRelationType()
        retVal.setExpression(newCTFactory().createSymbRef(sr))

        retVal
    }

    static FixedEffectRelationType createFixedEffectRelationScalar(JAXBElement<?> sr){
        def retVal = newMDefnFactory().createFixedEffectRelationType()
        retVal.setExpression(sr)

        retVal
    }

    static JAXBElement<CorrelationType> createRvCorrelationCorr(RhsType corrCoeff, SymbolRefType rv1, SymbolRefType rv2){
        def retVal = newMDefnFactory().createCorrelationType()
        retVal.pairwise = newMDefnFactory().createPairwiseType()
        retVal.pairwise.correlationCoefficient = newCTFactory().createStandardAssignType()
        retVal.pairwise.correlationCoefficient.assign = corrCoeff
        retVal.pairwise.randomVariable1 = newMDefnFactory().createCorrelatedRandomVarType()
        retVal.pairwise.randomVariable1.symbRef = rv1
        retVal.pairwise.randomVariable2 = newMDefnFactory().createCorrelatedRandomVarType()
        retVal.pairwise.randomVariable2.symbRef = rv2

        newMDefnFactory().createCommonParameterModelTypeCorrelation(retVal)
    }

    static JAXBElement<CorrelationType> createRvCorrelationCov(RhsType covar, SymbolRefType rv1, SymbolRefType rv2){
        def retVal = newMDefnFactory().createCorrelationType()
        retVal.pairwise = newMDefnFactory().createPairwiseType()
        retVal.pairwise.covariance = newCTFactory().createStandardAssignType()
        retVal.pairwise.covariance.assign = covar
        retVal.pairwise.randomVariable1 = newMDefnFactory().createCorrelatedRandomVarType()
        retVal.pairwise.randomVariable1.symbRef = rv1
        retVal.pairwise.randomVariable2 = newMDefnFactory().createCorrelatedRandomVarType()
        retVal.pairwise.randomVariable2.symbRef = rv2

        newMDefnFactory().createCommonParameterModelTypeCorrelation(retVal)
    }

}
