/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.modeldefn.UncertMLType
import eu.ddmore.libpharmml08.dom.uncertml.*
import foundation.ddmore.pharmml08.dom.uncertml.AbstractCategoricalUnivariateDistributionType
import foundation.ddmore.pharmml08.dom.uncertml.AbstractContinuousUnivariateDistributionType
import foundation.ddmore.pharmml08.dom.uncertml.AbstractDiscreteUnivariateDistributionType
import foundation.ddmore.pharmml08.dom.uncertml.ContinuousValueType
import foundation.ddmore.pharmml08.dom.uncertml.NormalDistribution
import foundation.ddmore.pharmml08.dom.uncertml.PositiveRealValueType
import foundation.ddmore.pharmml08.dom.uncertml.VarRefType

import static FactoryFactory.newUncertMlFactory

/**
 * Created by stumoodie on 04/01/2017.
 */
class UncertMlBuilder {

    static UncertMLType createUncertml(node){
        def retVal = new UncertMLType()
        def of = newUncertMlFactory()
        switch(node.class){
            case AbstractContinuousUnivariateDistributionType.class:
                retVal.setAbstractContinuousUnivariateDistribution(of.createAbstractContinuousUnivariateDistribution(node))
                break
            case AbstractDiscreteUnivariateDistributionType.class:
                retVal.setAbstractDiscreteUnivariateDistribution(of.createAbstractDiscreteUnivariateDistribution(node))
                break
            case AbstractCategoricalUnivariateDistributionType.class:
                retVal.setAbstractCategoricalUnivariateDistribution(of.createAbstractCategoricalUnivariateDistribution(node))
                break
            default:
                throw new IllegalArgumentException("Unrecognised uncertml distn: " + node.class)
        }

        retVal
    }

    static NormalDistribution createNormalDistnSd(ContinuousValueType mean, PositiveRealValueType stddev){
        def retVal = new NormalDistribution()
        retVal.mean = mean
        retVal.stddev = stddev
        retVal
    }

    static NormalDistribution createNormalDistnVar(ContinuousValueType mean, PositiveRealValueType variance){
        def retVal = new NormalDistribution()
        retVal.mean = mean
        retVal.variance = variance
        retVal
    }

    static ContinuousValueType createContinuousValue(Double value){
        def retVal = new ContinuousValueType()
        retVal.RVal = value
        retVal
    }

    static ContinuousValueType createContinuousValue(VarRefType value){
        def retVal = new ContinuousValueType()
        retVal.var = value
        retVal
    }

    static VarRefType createVarRef(String id){
        def retVal = new VarRefType()
        retVal.varId = id
        retVal
    }

    static PositiveRealValueType createPositiveRealValue(Double value){
        def retVal = new PositiveRealValueType()
        retVal.prVal = value
        retVal
    }

    static PositiveRealValueType createPositiveRealValue(VarRefType value){
        def retVal = new PositiveRealValueType()
        retVal.var = value
        retVal
    }

}
