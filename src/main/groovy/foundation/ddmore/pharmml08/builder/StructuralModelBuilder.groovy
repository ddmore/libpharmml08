/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.commontypes.DerivativeVariableType
import foundation.ddmore.pharmml08.dom.commontypes.IndependentVariableReferenceType
import foundation.ddmore.pharmml08.dom.commontypes.InitialConditionType
import foundation.ddmore.pharmml08.dom.commontypes.RegressorType
import foundation.ddmore.pharmml08.dom.commontypes.RhsType
import foundation.ddmore.pharmml08.dom.commontypes.StandardAssignType
import foundation.ddmore.pharmml08.dom.commontypes.SymbolRefType
import foundation.ddmore.pharmml08.dom.commontypes.SymbolTypeType
import foundation.ddmore.pharmml08.dom.commontypes.VariableDefinitionType
import foundation.ddmore.pharmml08.dom.modeldefn.AbstractPkMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.PkMacrosType
import foundation.ddmore.pharmml08.dom.modeldefn.StructuralModelType
import foundation.ddmore.pharmml08.dom.modeldefn.ObjectFactory

import javax.xml.bind.JAXBElement

import static FactoryFactory.*

/**
 * Created by stumoodie on 30/01/2017.
 */
class StructuralModelBuilder {

    static StructuralModelType createStructuralModel(String name, def ... structModelElements){
        def retVal = newMDefnFactory().createStructuralModelType()
        retVal.blkId = name

        structModelElements.each {se->
            retVal.getPopulationParameterAndIndividualParameterAndParameter().add(se)
        }

        retVal
    }


    static JAXBElement<VariableDefinitionType> createVariable(String name, RhsType rhs = null){
        def of = new foundation.ddmore.pharmml08.dom.commontypes.ObjectFactory()
        def retVal = of.createVariableDefinitionType()

        retVal.symbId = name
        retVal.symbolType = SymbolTypeType.REAL
        retVal.regressor = RegressorType.NO
        if(rhs){
            retVal.assign = rhs
        }

        of.createVariable(retVal)
    }


    static JAXBElement<DerivativeVariableType> createDerivative(String symbId, RhsType rhs, RhsType initVal = null, RhsType initTime = null, String idv = null){
        def of = newCTFactory()
        def retVal = of.createDerivativeVariableType()

        retVal.symbId = symbId
        retVal.assign = rhs
        retVal.symbolType = SymbolTypeType.REAL
        if(initVal || initTime){
            retVal.initialCondition = of.createInitialConditionType()
        }
        if(initVal){
            retVal.initialCondition.initialValue = of.createStandardAssignType()
            retVal.initialCondition.initialValue.assign = initVal
        }
        if(initTime){
            retVal.initialCondition.initialTime = of.createStandardAssignType()
            retVal.initialCondition.initialTime.assign = initTime
        }
        if(idv) {
            retVal.independentVariable = of.createIndependentVariableReferenceType()
            retVal.independentVariable.symbRef = of.createSymbolRefType()
            retVal.independentVariable.symbRef.symbIdRef = idv
        }

        of.createDerivativeVariable(retVal)
    }

    static PkMacrosType createPkMacro(AbstractPkMacroType ... pkMacros){
        def of = new ObjectFactory()
        def retVal = of.createPkMacrosType()

        pkMacros.each {p->
            retVal.getAbstractPkMacro().add(p)
        }

        retVal
    }


    static private createMacro(def macro, List valueTypes, List<RhsType> rhs, Closure valObjConstructor){
        for(int i = 0; i < valueTypes.size(); i++){
            def mdOf = newMDefnFactory()
            def val = valObjConstructor.call()
            val.setArgument(valueTypes.get(i))
            val.setAssign(rhs.get(i))
            macro.getValue().add((val))
        }
        macro
    }

    static private objFact(){
        new ObjectFactory()
    }

    static createAbsorptionMacro(List valueTypes, List<RhsType> rhs){
        createMacro(objFact().createAbsorptionMacroType(), valueTypes, rhs, {objFact().createAbsorptionOralMacroValueType()})
    }

    static createOralMacro(List valueTypes, List<RhsType> rhs){
        createMacro(objFact().createOralMacroType(), valueTypes, rhs, {objFact().createAbsorptionOralMacroValueType()})
    }

    static createIvMacro(List valueTypes, List<RhsType> rhs){
        createMacro(objFact().createIVMacroType(), valueTypes, rhs, {objFact().createIVMacroValueType()})
//        def of = new eu.ddmore.libpharmml.dom.modeldefn.ObjectFactory()
//        def ivMacro = objFact().createIVMacroType()
//        for(def i; i < valueTypes.size(); i++){
//            def mdOf = new eu.ddmore.libpharmml.dom.modeldefn.ObjectFactory()
//            def val = mdOf.createIVMacroValueType()
//            val.setArgument(valueTypes[i])
//            val.setAssign(rhs[i])
//            ivMacro.getValue().add((val))
//        }
//        ivMacro
    }

    static createCompartmentMacro(List valueTypes, List<RhsType> rhs){
        createMacro(objFact().createCompartmentMacroType(), valueTypes, rhs, {objFact().createCompartmentMacroValueType()})
    }

    static createDepotMacro(List valueTypes, List<RhsType> rhs){
        createMacro(objFact().createDepotMacroType(), valueTypes, rhs, {objFact().createDepotMacroValueType()})
    }

    static createEliminationMacro(List valueTypes, List<RhsType> rhs){
        createMacro(objFact().createEliminationMacroType(), valueTypes, rhs, {objFact().createEliminationMacroValueType()})
    }

    static createTransferMacro(List valueTypes, List<RhsType> rhs){
        createMacro(objFact().createTransferMacroType(), valueTypes, rhs, {objFact().createTransferMacroValueType()})
    }

    static createEffectMacro(List valueTypes, List<RhsType> rhs){
        createMacro(objFact().createEffectMacroType(), valueTypes, rhs, {objFact().createEffectMacroValueType()})
    }

    static createPeripheralMacro(List valueTypes, List<RhsType> rhs){
        createMacro(objFact().createPeripheralMacroType(), valueTypes, rhs, {objFact().createPeripheralMacroValueType()})
    }

//    static createMacroValue(String arg, RhsType value){
//        def retVal = new MacroValue()
//
//        retVal.argument = arg
//        retVal.assign = value
//
//        retVal
//    }

}
