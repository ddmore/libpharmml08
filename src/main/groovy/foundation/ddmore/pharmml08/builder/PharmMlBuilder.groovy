/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.commontypes.FuncParameterDefinitionType
import foundation.ddmore.pharmml08.dom.commontypes.PharmMLRootType
import foundation.ddmore.pharmml08.dom.commontypes.RhsType
import foundation.ddmore.pharmml08.dom.commontypes.SymbolTypeType
import foundation.ddmore.pharmml08.dom.modeldefn.StructuredModelType
import groovy.transform.TypeChecked

import javax.xml.bind.JAXBElement

import static foundation.ddmore.pharmml08.builder.FactoryFactory.*
import foundation.ddmore.pharmml08.dom.IndependentVariableType
import foundation.ddmore.pharmml08.dom.PharmML
import foundation.ddmore.pharmml08.dom.commontypes.FunctionDefinitionType
import foundation.ddmore.pharmml08.dom.modeldefn.CovariateModelType
import foundation.ddmore.pharmml08.dom.modeldefn.ModelDefinitionType
import foundation.ddmore.pharmml08.dom.modeldefn.ObservationModelType
import foundation.ddmore.pharmml08.dom.modeldefn.ParameterModelType
import foundation.ddmore.pharmml08.dom.modeldefn.StructuralModelType
import foundation.ddmore.pharmml08.dom.modeldefn.VariabilityDefnBlock
import foundation.ddmore.pharmml08.dom.modellingsteps.ModellingStepsType
import foundation.ddmore.pharmml08.dom.trialdesign.TrialDesignType

/**
 * Created by stumoodie on 04/01/2017.
 */
@TypeChecked
class PharmMlBuilder {

    static createPharmMl(String name, List<String> idvNames, List<FunctionDefinitionType> fds = Collections.emptyList(), ModelDefinitionType md = null,
                         TrialDesignType td = null, ModellingStepsType ms = null, String description = null){
        def fact = newPharmMLFactory()
        def retVal = fact.createPharmML()
        def nm = newCTFactory().createNameType()
        nm.value = name
        retVal.name = nm
        if(description){
            def annot = newCTFactory().createAnnotationType()
            annot.value = description
            retVal.description = annot
        }
        idvNames.each {idvName->
            def idv = fact.createIndependentVariableType()
            idv.symbId = idvName
            retVal.getIndependentVariable().add(idv)
        }
        retVal.getFunctionDefinition().addAll(fds)
        retVal.modelDefinition = md
        retVal.trialDesign = td
        retVal.modellingSteps = ms

        retVal
    }

    static ModelDefinitionType createModelDefinition(PharmMLRootType ... nodes){
        def retVal = newMDefnFactory().createModelDefinitionType()
        for(def defn : nodes){
            switch(defn.class){
                case CovariateModelType.class:
                    retVal.getCovariateModel().add((CovariateModelType)defn)
                    break
                case ParameterModelType.class:
                    retVal.getParameterModel().add((ParameterModelType)defn)
                    break
                case StructuralModelType.class:
                    retVal.getStructuralModel().add((StructuralModelType)defn)
                    break
                case VariabilityDefnBlock.class:
                    retVal.getVariabilityModel().add((VariabilityDefnBlock)defn)
                    break
                case ObservationModelType.class:
                    retVal.getObservationModel().add((ObservationModelType)defn)
                    break
                default:
                    throw new IllegalArgumentException("Unrecognised type: " + defn.class.toString())
            }
        }

        retVal
    }

    static FunctionDefinitionType createFunctionDefinition(String fName, SymbolTypeType returnType, RhsType defn,
                                                           FuncParameterDefinitionType ... args){
        def retVal = newCTFactory().createFunctionDefinitionType()

        retVal.symbId = fName
        retVal.symbolType = returnType
        retVal.definition = newCTFactory().createStandardAssignType()
        retVal.definition.assign = defn
        args.each {a->
            retVal.getFunctionArgument().add((FuncParameterDefinitionType)a)
        }

        retVal
    }


    static FuncParameterDefinitionType createFunctionParameter(String paramName, SymbolTypeType paramType){
        def retVal = newCTFactory().createFuncParameterDefinitionType()

        retVal.symbId = paramName
        retVal.symbolType = paramType

        retVal
    }

    static TrialDesignType createTrialDesign(){
        newDesignFactory().createTrialDesignType()
    }

    static ModellingStepsType createModellingSteps(){
        newModStepsFactory().createModellingStepsType()
    }
}
