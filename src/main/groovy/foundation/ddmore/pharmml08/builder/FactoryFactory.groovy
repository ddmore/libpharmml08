/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import groovy.transform.TypeChecked

/**
 * Created by stumoodie on 22/02/2017.
 */
@TypeChecked
class FactoryFactory {

    static foundation.ddmore.pharmml08.dom.ObjectFactory newPharmMLFactory(){
        return new foundation.ddmore.pharmml08.dom.ObjectFactory()
    }

    static foundation.ddmore.pharmml08.dom.commontypes.ObjectFactory newCTFactory(){
        return new foundation.ddmore.pharmml08.dom.commontypes.ObjectFactory()
    }

    static foundation.ddmore.pharmml08.dom.modeldefn.ObjectFactory newMDefnFactory(){
        return new foundation.ddmore.pharmml08.dom.modeldefn.ObjectFactory()
    }

    static foundation.ddmore.pharmml08.dom.maths.ObjectFactory newMathsFactory(){
        return new foundation.ddmore.pharmml08.dom.maths.ObjectFactory()
    }

    static foundation.ddmore.pharmml08.dom.modellingsteps.ObjectFactory newModStepsFactory(){
        return new foundation.ddmore.pharmml08.dom.modellingsteps.ObjectFactory()
    }

    static foundation.ddmore.pharmml08.dom.uncertml.ObjectFactory newUncertMlFactory(){
        return new foundation.ddmore.pharmml08.dom.uncertml.ObjectFactory()
    }

    static foundation.ddmore.pharmml08.dom.probonto.ObjectFactory newProbontoFactory(){
        return new foundation.ddmore.pharmml08.dom.probonto.ObjectFactory()
    }

    static foundation.ddmore.pharmml08.dom.trialdesign.ObjectFactory newDesignFactory(){
        return new foundation.ddmore.pharmml08.dom.trialdesign.ObjectFactory()
    }

    static foundation.ddmore.pharmml08.dom.dataset.ObjectFactory newDatasetFactory(){
        return new foundation.ddmore.pharmml08.dom.dataset.ObjectFactory()
    }

}
