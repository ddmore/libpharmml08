/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.commontypes.PharmMLRootType
import foundation.ddmore.pharmml08.dom.commontypes.RhsType
import eu.ddmore.libpharmml08.dom.modeldefn.*
import foundation.ddmore.pharmml08.dom.modeldefn.CategoricalCovariateType
import foundation.ddmore.pharmml08.dom.modeldefn.CategoryType
import foundation.ddmore.pharmml08.dom.modeldefn.CommonParameterType
import foundation.ddmore.pharmml08.dom.modeldefn.ContinuousCovariateType
import foundation.ddmore.pharmml08.dom.modeldefn.CovariateDefinitionType
import foundation.ddmore.pharmml08.dom.modeldefn.CovariateModelType
import foundation.ddmore.pharmml08.dom.modeldefn.CovariateTransformationType
import foundation.ddmore.pharmml08.dom.modeldefn.CovariateType
import foundation.ddmore.pharmml08.dom.modeldefn.DistributionType
import foundation.ddmore.pharmml08.dom.modeldefn.ObjectFactory
import foundation.ddmore.pharmml08.dom.modeldefn.PopulationParameterType
import foundation.ddmore.pharmml08.dom.modeldefn.SimpleParameterType
import foundation.ddmore.pharmml08.dom.modeldefn.TransformedCovariateType
import groovy.transform.TypeChecked

/**
 * Created by stumoodie on 04/01/2017.
 */
@TypeChecked
class CovariateModelBuilder {

    static CovariateModelType createCovariateModel(String blkId, PharmMLRootType ... defns){
        def retVal = new CovariateModelType()
        retVal.blkId = blkId
        for(PharmMLRootType defn : defns){
            switch(defn.class){
                case CovariateDefinitionType.class:
                    retVal.getCovariate().add((CovariateDefinitionType)defn)
                    break
                case PopulationParameterType.class:
                    retVal.getPopulationParameter().add((PopulationParameterType)defn)
                    break
                case SimpleParameterType.class:
                    retVal.getParameter().add((SimpleParameterType)defn)
                    break
                default:
                    throw new IllegalArgumentException("Unrecognised definition type: " + defn.class.toString())
            }
        }

        retVal
    }

    static CovariateDefinitionType createCovariateDefinition(String symbId, PharmMLRootType covRhs, CovariateType covType = CovariateType.CONSTANT){
        CovariateDefinitionType retVal = new CovariateDefinitionType()
        retVal.symbId = symbId
        retVal.type = covType
        if(covRhs.class == ContinuousCovariateType){
            retVal.setContinuous((ContinuousCovariateType)covRhs)
        }
        else if(covRhs.class == CategoricalCovariateType){
            retVal.setCategorical((CategoricalCovariateType)covRhs)
        }

        retVal
    }

    static ContinuousCovariateType createContinuousCovariate(){
        createContinuousCovariate(null)
    }
    static ContinuousCovariateType createContinuousCovariate(PharmMLRootType rhs, List<CovariateTransformationType> covTransfrom = Collections.emptyList()){
        def retVal = FactoryFactory.newMDefnFactory().createContinuousCovariateType()
        if(rhs?.class == RhsType.class){
            retVal.setAssign((RhsType)rhs)
        }
        else if(rhs?.class == DistributionType.class){
            retVal.setDistribution((DistributionType)rhs)
        }
        covTransfrom.each { t ->
            retVal.getTransformation().add(t)
        }

        retVal
    }

    static CategoricalCovariateType createCategoricalCovariate(CategoryType ... categories){
        def retVal = new CategoricalCovariateType()
        categories.each { cat ->
            retVal.getCategory().add((CategoryType)cat)
        }

        retVal
    }

    static CategoryType createCategory(String name){
        def retVal = new CategoryType()
        def nmTyp = FactoryFactory.newCTFactory().createNameType()
        nmTyp.setValue(name)
        retVal.setName(nmTyp)
        retVal
    }

    static CovariateTransformationType createTransformedCovariate(String name, RhsType rhs){
        def of = new ObjectFactory()
        def retVal = of.createCovariateTransformationType()
        retVal.transformedCovariate = new TransformedCovariateType()
        retVal.transformedCovariate.symbId = name
        retVal.assign = rhs
        retVal
    }


}
