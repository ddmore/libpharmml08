/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.maths.UnaryOperator
import foundation.ddmore.pharmml08.dom.probonto.DistroNameType
import foundation.ddmore.pharmml08.dom.probonto.DistroType
import foundation.ddmore.pharmml08.dom.probonto.ParameterNameType
import org.junit.Test

import static MathsBuilder.createDistribution
import static org.junit.Assert.assertNotNull

/**
 * Created by stumoodie on 05/01/2017.
 */
class CovariateModelBuilderTest {


    @Test
    void testSingleSimpleDefinition(){
        def root = PharmMlBuilder.createModelDefinition(
                CovariateModelBuilder.createCovariateModel("cm",
                    CovariateModelBuilder.createCovariateDefinition(
                            "W",
                            CovariateModelBuilder.createContinuousCovariate()
                    ),
            )
        )

        assertNotNull(root)
    }


    @Test
    void testMultipleDefinitions(){
        def root = PharmMlBuilder.createModelDefinition(CovariateModelBuilder.createCovariateModel("cm",
                CovariateModelBuilder.createCovariateDefinition(
                    "W",
                    CovariateModelBuilder.createContinuousCovariate()
                ),
                CovariateModelBuilder.createCovariateDefinition(
                        "Y",
                        CovariateModelBuilder.createContinuousCovariate(
                                MathsBuilder.createRhs(
                                        MathsBuilder.createReal(0)
                                )
                        )
                ),
                CovariateModelBuilder.createCovariateDefinition(
                        "Z",
                        CovariateModelBuilder.createContinuousCovariate(
                                createDistribution(
                                        ProbontoBuilder.createProbonto(DistroNameType.NORMAL_1, DistroType.UNIVARIATE,
                                                ProbontoBuilder.createDistnParameter(ParameterNameType.MEAN, MathsBuilder.createRhs(
                                                        MathsBuilder.createReal(0)
                                                )),
                                                ProbontoBuilder.createDistnParameter(ParameterNameType.STDEV, MathsBuilder.createRhs(
                                                        MathsBuilder.createReal(1)
                                                ))
                                        )
                                )
                        )
                )
        )
        )

        assertNotNull(root)
    }

    @Test
    void testMultipleModels(){
        def root = PharmMlBuilder.createModelDefinition(
                CovariateModelBuilder.createCovariateModel("cm",
                    CovariateModelBuilder.createCovariateDefinition(
                            "W",
                            CovariateModelBuilder.createContinuousCovariate()
                    ),
                    CovariateModelBuilder.createCovariateDefinition(
                            "Y",
                            CovariateModelBuilder.createContinuousCovariate(
                                    MathsBuilder.createRhs(
                                            MathsBuilder.createReal(0)
                                    )
                            )
                    ),
                    CovariateModelBuilder.createCovariateDefinition(
                            "Z",
                            CovariateModelBuilder.createContinuousCovariate(
                                    createDistribution(
                                            ProbontoBuilder.createProbonto(DistroNameType.NORMAL_1, DistroType.UNIVARIATE,
                                                    ProbontoBuilder.createDistnParameter(ParameterNameType.MEAN, MathsBuilder.createRhs(
                                                            MathsBuilder.createReal(0)
                                                    )),
                                                    ProbontoBuilder.createDistnParameter(ParameterNameType.STDEV, MathsBuilder.createRhs(
                                                            MathsBuilder.createReal(1)
                                                    ))
                                            )
                                    )
                            )
                    )
                )
        )

        assertNotNull(root)
    }

    @Test
    void testModelWithCovTransformation(){
        def root = PharmMlBuilder.createModelDefinition(
                CovariateModelBuilder.createCovariateModel("cm",
                        CovariateModelBuilder.createCovariateDefinition(
                                "W",
                                CovariateModelBuilder.createContinuousCovariate(null,
                                    [CovariateModelBuilder.createTransformedCovariate(
                                            'logW',
                                            MathsBuilder.createRhs(
                                                    MathsBuilder.createUniop(
                                                            UnaryOperator.LOG,
                                                            MathsBuilder.createSymbRef("W")
                                                    )
                                            )
                                    )]
                                )
                        ),
                )
        )

        assertNotNull(root)
    }

    @Test
    void testMultipleContCatModels(){
        def root = PharmMlBuilder.createModelDefinition(
                CovariateModelBuilder.createCovariateModel("cm",
                        CovariateModelBuilder.createCovariateDefinition(
                                "W",
                                CovariateModelBuilder.createContinuousCovariate()
                        ),
                        CovariateModelBuilder.createCovariateDefinition(
                                "S",
                                CovariateModelBuilder.createCategoricalCovariate(
                                        CovariateModelBuilder.createCategory('a'),
                                        CovariateModelBuilder.createCategory('b'),
                                        CovariateModelBuilder.createCategory('c')
                                )
                        ),
                )
        )

        assertNotNull(root)
    }

}
