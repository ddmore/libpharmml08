/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.commontypes.MatrixType
import foundation.ddmore.pharmml08.dom.commontypes.MatrixTypeType
import foundation.ddmore.pharmml08.dom.maths.BinaryOperator
import foundation.ddmore.pharmml08.dom.maths.ConstantValueType
import foundation.ddmore.pharmml08.dom.maths.LogicalBinOperator
import foundation.ddmore.pharmml08.dom.maths.MatrixUniOperatorType
import foundation.ddmore.pharmml08.dom.maths.UnaryOperator
import org.junit.Test

import static MathsBuilder.*
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotNull

/**
 * Created by stumoodie on 04/01/2017.
 */
class MathMlBuilderTest {

    @Test
    void testScalarInt(){
        def root = createRhs(
                createInt(22)
        )

        assertNotNull(root)
    }

    @Test
    void testScalarReal(){
        def root = createRhs(
                createReal(23.55)
        )

        assertNotNull(root)
    }

    @Test
    void testScalarVerySmallReal(){
        def root = createRhs(
                createReal(2.355e-10)
        )

        assertNotNull(root)
    }

    @Test
    void testScalarBooleanTrue(){
        def root = createRhs(
                createBoolean(true)
        )

        assertNotNull(root)
    }

    @Test
    void testScalarBooleanFalse(){
        def root = createRhs(
                createBoolean(false)
        )

        assertNotNull(root)
    }

    @Test
    void testScalarInfinity(){
        def root = createRhs(
            createPlusInf()
        )

        assertNotNull(root)
    }

    @Test
    void testScalarNegInfinity(){
        def root = createRhs(
               createMinusInf()
        )

        assertNotNull(root)
    }

    @Test
    void testConstantPi(){
        def root = createRhs(
                createConstant(ConstantValueType.PI)
        )

        assertNotNull(root)
    }

    @Test
    void testConstantExp(){
        def root = createRhs(
                createConstant(ConstantValueType.EXPONENTIALE)
        )

        assertNotNull(root)
    }

    @Test
    void testSimpleBinOp(){
        def root = createAssign(
                createBinop(
                        BinaryOperator.PLUS,
                        createReal(22.0),
                        createInt(33)
                )
        )

        assertNotNull(root)
    }

    @Test
    void testSimpleBinOpNoMaths(){
        def root = createRhs(
                createBinop(
                        BinaryOperator.PLUS,
                        createReal(22.0),
                        createInt(33)
                )
        )

        assertNotNull(root)
    }

    @Test
    void testRightNestedBinOp(){
        def root = createRhs(
                createBinop(
                        BinaryOperator.PLUS,
                        createReal(22.0),
                        createBinop(
                                BinaryOperator.TIMES,
                                createSymbRef("x"),
                                createInt(33)
                        )
                )
        )

        assertNotNull(root)
    }

    @Test
    void testLeftNestedBinOp(){
        def root = createRhs(
                createBinop(
                        BinaryOperator.TIMES,
                        createBinop(
                                BinaryOperator.PLUS,
                                createReal(22.0),
                                createSymbRef("x")
                        ),
                        createInt(33)

                )
        )

        assertNotNull(root)
    }

    @Test
    void testSimpleUniOp(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.MINUS,
                        createReal(22.0),
                )
        )

        assertNotNull(root)
    }

    @Test
    void testExponentialUniOp(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.EXP,
                        createBinop(
                                BinaryOperator.DIVIDE,
                                createSymbRef("x"),
                                createSymbRef("y")
                        )
                )
        )

        assertNotNull(root)
    }

    @Test
    void testSimpleSqrtUniOp(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.SQRT,
                        createReal(22.0),
                )
        )

        assertNotNull(root)
    }

    @Test
    void testMinusWithExpressionArgsUniOp(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.MINUS,
                        createBinop(
                                BinaryOperator.POWER,
                                createSymbRef("x"),
                                createInt(33)
                        )
                )
        )
        assertNotNull(root)
    }

    @Test
    void testAbsWithExpressionsArgs(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.ABS,
                        createBinop(
                                BinaryOperator.POWER,
                                createSymbRef("x"),
                                createInt(33)
                        )
                )
        )
        assertNotNull(root)
    }

    @Test
    void testFactorialWithSumArgs(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.FACTORIAL,
                        createBinop(
                                BinaryOperator.MINUS,
                                createSymbRef("x"),
                                createInt(33)
                        )
                )
        )
        assertNotNull(root)
    }

    @Test
    void testSinWithSumExpr(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.SIN,
                        createBinop(
                                BinaryOperator.MINUS,
                                createSymbRef("x"),
                                createInt(33)
                        )
                )
        )
        assertNotNull(root)
    }

    @Test
    void testLn(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.LOG,
                        createReal(22.5)
                )
        )

        assertNotNull(root)
    }

    @Test
    void testLn2(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.LOG_2,
                        createReal(22.5)
                )
        )

        assertNotNull(root)
    }

    @Test
    void testLn10(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.LOG_10,
                        createReal(44.4)
                )
        )

        assertNotNull(root)
    }
    @Test
    void testComplexEquation(){
        def root = createRhs(
                createBinop(
                        BinaryOperator.MINUS,
                        createSymbRef("sm", "RATEIN"),
                        createBinop(
                                BinaryOperator.DIVIDE,
                                createBinop(
                                        BinaryOperator.TIMES,
                                        createSymbRef("pm", "CL"),
                                        createSymbRef("CENTRAL")
                                ),
                                createSymbRef("pm", "V")
                        )
                )
        )

        assertNotNull(root)
    }

    @Test
    void testPricewiseEquation(){
        def root = createRhs(
                createPiecewise(
                        createPiece(
                                createCondition(
                                        createLogicBinop(
                                                LogicalBinOperator.GEQ,
                                                createSymbRef("T"),
                                                createSymbRef("pm", "TLAG")
                                        )
                                ),
                                createBinop(
                                        BinaryOperator.TIMES,
                                        createSymbRef("sm", "GUT"),
                                        createSymbRef("pm", "KA")
                                )
                        ),
                        createPiece(
                                createCondition(createOtherwise()),
                                createInt(0)
                        )
                )
        )

        assertNotNull(root)
    }

    @Test
    void testPricewiseSingleColumnRefEquation(){
        def root = createRhs(
                createPiecewise(
                        createPiece(
                                createCondition(
                                        createLogicBinop(
                                                LogicalBinOperator.GT,
                                                createColumnRef("AMT"),
                                                createInt(0)
                                        )
                                ),
                                createSymbRef("sm", "GUT"),
                        )
                )
        )

        assertNotNull(root)
    }

    @Test
    void testVectorDefinition(){
        def root = createRhs(
                createVector(
                        createReal(0),
                        createReal(1),
                        createReal(2)
                )
        )

        assertNotNull(root)
    }


    @Test
    void testVectorSelector(){
        def root = createRhs(
                createVectorSelector(
                        createSymbRefType("v"),
                        createRhs(
                            createInt(3)
                        )
                )
        )

        assertNotNull(root)
    }

    @Test
    void testMatrixDefinition(){
        def root = createRhs(
                createMatrix(
                        MatrixTypeType.ANY,
                        createMatrixRow(
                            createReal(11),
                            createReal(12),
                            createReal(13)
                        ),
                        createMatrixRow(
                                createReal(21),
                                createReal(22),
                                createReal(23)
                        )
                )
        )

        assertNotNull(root)

        def matVal = root.expression.value as MatrixType

        assertEquals(MatrixTypeType.ANY, matVal.matrixType)
        assertEquals(11, matVal.matrixColumnOrMatrixRowOrMatrixCell[0].getExpressionOrAssign()[0].value.value, 0.0001)
        assertEquals(23, matVal.matrixColumnOrMatrixRowOrMatrixCell[1].getExpressionOrAssign()[2].value.value, 0.0001)
    }

    @Test
    void testMatrixUniOp(){
        def root = createRhs(
                createMatrixUniOp(MatrixUniOperatorType.INVERSE,
                    createMatrix(
                            MatrixTypeType.ANY,
                            createMatrixRow(
                                    createReal(11),
                                    createReal(12),
                                    createReal(13)
                            ),
                            createMatrixRow(
                                    createReal(21),
                                    createReal(22),
                                    createReal(23)
                            )
                    )
                )
        )

        assertNotNull(root)

        assertEquals(MatrixUniOperatorType.INVERSE, root.expression.value.op)
        def matVal = root.expression.value.expression.value as MatrixType

        assertEquals(MatrixTypeType.ANY, matVal.matrixType)
        assertEquals(11, matVal.matrixColumnOrMatrixRowOrMatrixCell[0].getExpressionOrAssign()[0].value.value, 0.0001)
        assertEquals(23, matVal.matrixColumnOrMatrixRowOrMatrixCell[1].getExpressionOrAssign()[2].value.value, 0.0001)
    }

}
