/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.maths.BinaryOperator
import foundation.ddmore.pharmml08.dom.maths.UnaryOperator
import foundation.ddmore.pharmml08.dom.modeldefn.TransformationType
import foundation.ddmore.pharmml08.dom.probonto.DistroNameType
import foundation.ddmore.pharmml08.dom.probonto.DistroType
import foundation.ddmore.pharmml08.dom.probonto.ParameterNameType
import org.junit.Test

import static MathsBuilder.createDistribution
import static ParameterModelBuilder.createStructuredModel
import static org.junit.Assert.assertNotNull

/**
 * Created by stumoodie on 05/01/2017.
 */
class ParameterBuilderTest {

    @Test
    void testSimpleParameterNoRhs(){
        def root = ParameterModelBuilder.createSimpleParam("pop")

        assertNotNull(root)
    }

    @Test
    void testSimpleParameterRhs(){
        def root = ParameterModelBuilder.createSimpleParam(
                "pop",
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(0.0)
                )
        )

        assertNotNull(root)
    }

    @Test
    void testPopulationParameterNoRhs(){
        def root = ParameterModelBuilder.createPopulationParam("pop")

        assertNotNull(root)
    }

    @Test
    void testPopulationParameterRhs(){
        def root = ParameterModelBuilder.createPopulationParam(
                "pop",
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(0.0)
                )
        )

        assertNotNull(root)
    }

    @Test
    void testPopulationParameterComplexRhs(){
        def root = ParameterModelBuilder.createPopulationParam(
                "Rin",
                MathsBuilder.createRhs(
                        MathsBuilder.createBinop(
                                BinaryOperator.TIMES,
                                MathsBuilder.createSymbRef("pop_Rin"),
                                MathsBuilder.createUniop(
                                        UnaryOperator.EXP,
                                        MathsBuilder.createSymbRef("eta_Rin")
                                )
                        )
                )
        )

        assertNotNull(root)
    }

    @Test
    void testPopulationParameterDistribution(){
        def root = ParameterModelBuilder.createPopulationParam(
                "pop",
                createDistribution(
                        ProbontoBuilder.createProbonto(
                                DistroNameType.WISHART_1,
                                DistroType.UNIVARIATE,
                                ProbontoBuilder.createDistnParameter(
                                        ParameterNameType.DEGREES_OF_FREEDOM,
                                        MathsBuilder.createRhs(
                                                MathsBuilder.createInt(3)
                                        )
                                )
                        )
                ),
                ParameterModelBuilder.createVariabilityRef("vm", "prior")
        )

        assertNotNull(root)
    }

    @Test
    void testRandomVariable(){
        def root = ParameterModelBuilder.createRandomVariable(
                "rv",
                createDistribution(
                        ProbontoBuilder.createProbonto(
                                DistroNameType.WISHART_1,
                                DistroType.UNIVARIATE,
                                ProbontoBuilder.createDistnParameter(
                                        ParameterNameType.DEGREES_OF_FREEDOM,
                                        MathsBuilder.createRhs(
                                                MathsBuilder.createInt(3)
                                        )
                                )
                        )
                ),
                [ParameterModelBuilder.createVariabilityRef("vm", "prior") ]
        )

        assertNotNull(root)
    }

    @Test
    void testRandomVariableWithTransform(){
        def root = ParameterModelBuilder.createRandomVariable(
                "rv",
                createDistribution(
                        ProbontoBuilder.createProbonto(
                                DistroNameType.WISHART_1,
                                DistroType.UNIVARIATE,
                                ProbontoBuilder.createDistnParameter(
                                        ParameterNameType.DEGREES_OF_FREEDOM,
                                        MathsBuilder.createRhs(
                                                MathsBuilder.createInt(3)
                                        )
                                )
                        )
                ),
                [ParameterModelBuilder.createVariabilityRef("vm", "prior") ],
                ParameterModelBuilder.createRVTransform(TransformationType.LOG)
        )

        assertNotNull(root)
    }

    @Test
    void testRandomVariableMultiVarLevels(){
        def root = ParameterModelBuilder.createRandomVariable(
                "rv",
                createDistribution(
                        ProbontoBuilder.createProbonto(
                                DistroNameType.NORMAL_1,
                                DistroType.UNIVARIATE,
                                ProbontoBuilder.createDistnParameter(
                                        ParameterNameType.MEAN,
                                        MathsBuilder.createRhs(
                                                MathsBuilder.createSymbRef("pop")
                                        )
                                )
                        )
                ),
                [ParameterModelBuilder.createVariabilityRef("vm", "p"),
                 ParameterModelBuilder.createVariabilityRef("vm", "q")]
        )

        assertNotNull(root)
    }

    @Test
    void testIndividualParameterExplicity(){
        def root = ParameterModelBuilder.createIndividualParameter(
                "idv",
                MathsBuilder.createRhs(MathsBuilder.createReal(2.2))
        )

        assertNotNull(root)
    }

    @Test
    void testIndividualParameterStandardLinearNoCovariate(){
        def root = ParameterModelBuilder.createStructuredIndividualParameter(
                "idv",
                createStructuredModel(
                        ParameterModelBuilder.createLinearCovariate(
                                MathsBuilder.createRhs(
                                    MathsBuilder.createSymbRef("pop")
                                ),
                                Collections.emptyList()
                        ),
                        [MathsBuilder.createSymbRefType("rv1")]
                )
        )

        assertNotNull(root)
    }

    @Test
    void testIndividualParameterStandardLinear(){
        def root = ParameterModelBuilder.createStructuredIndividualParameter(
                "idv",
                createStructuredModel(
                        ParameterModelBuilder.createLinearCovariate(
                                MathsBuilder.createRhs(
                                        MathsBuilder.createSymbRef("pop")
                                ),
                                [ParameterModelBuilder.createCovariateRelation(
                                        MathsBuilder.createSymbRefType("cm", "C1"),
                                        ParameterModelBuilder.createFixedEffectRelationParamRef(
                                                MathsBuilder.createSymbRefType("beta")
                                        )
                                )]
                        ),
                        [MathsBuilder.createSymbRefType("rv1")]
                )
        )

        assertNotNull(root)
    }

    @Test
    void testIndividualParameterStandardLinearWithTransform(){
        def root = ParameterModelBuilder.createStructuredIndividualParameter(
                "idv",
                createStructuredModel(
                        ParameterModelBuilder.createLinearCovariate(
                                MathsBuilder.createRhs(
                                        MathsBuilder.createSymbRef("pop")
                                ),
                                [ParameterModelBuilder.createCovariateRelation(
                                        MathsBuilder.createSymbRefType("cm", "C1"),
                                        ParameterModelBuilder.createFixedEffectRelationParamRef(
                                                MathsBuilder.createSymbRefType("beta")
                                        )
                                )]
                        ),
                        [MathsBuilder.createSymbRefType("rv1")],
                        ParameterModelBuilder.createParameterTransform(TransformationType.LOG)
                )
        )

        assertNotNull(root)
    }

    @Test
    void testIndividualParameterStandardLinear2Rvs(){
        def root = ParameterModelBuilder.createStructuredIndividualParameter(
                "idv",
                createStructuredModel(
                        ParameterModelBuilder.createLinearCovariate(
                                MathsBuilder.createRhs(
                                        MathsBuilder.createSymbRef("pop")
                                ),
                                [
                                        ParameterModelBuilder.createCovariateRelation(
                                            MathsBuilder.createSymbRefType("cm", "C1"),
                                            ParameterModelBuilder.createFixedEffectRelationParamRef(
                                                    MathsBuilder.createSymbRefType("beta")
                                            )
                                        ),
                                        ParameterModelBuilder.createCovariateRelation(
                                                MathsBuilder.createSymbRefType("cm", "C2"),
                                                ParameterModelBuilder.createFixedEffectRelationScalar(
                                                        MathsBuilder.createReal(2.9)
                                                )
                                        )
                                ]
                        ),
                        [
                                MathsBuilder.createSymbRefType("rv1"),
                                MathsBuilder.createSymbRefType("rv2")
                        ]
                )
        )

        assertNotNull(root)
    }

    @Test
    void testIndividualParameterStandardLinearCategoricalCovs(){
        def root = ParameterModelBuilder.createStructuredIndividualParameter(
                "idv",
                createStructuredModel(
                        ParameterModelBuilder.createLinearCovariate(
                                MathsBuilder.createRhs(
                                        MathsBuilder.createSymbRef("pop")
                                ),
                                [
                                        ParameterModelBuilder.createCovariateRelation(
                                                MathsBuilder.createSymbRefType("cm", "C1"),
                                                ParameterModelBuilder.createFixedEffectRelationCategorical(
                                                        MathsBuilder.createSymbRefType("beta"),
                                                        "male")
                                        )
                                ]
                        ),
                        [
                                MathsBuilder.createSymbRefType("rv1")
                        ]
                )
        )

        assertNotNull(root)
    }

    @Test
    void testIndividualParameterStandardGeneral(){
        def root = ParameterModelBuilder.createStructuredIndividualParameter(
                "idv",
                createStructuredModel(
                        ParameterModelBuilder.createGeneralCovariate(
                                MathsBuilder.createRhs(
                                        MathsBuilder.createBinop(
                                                BinaryOperator.DIVIDE,
                                                MathsBuilder.createSymbRef("cm", "C1"),
                                                MathsBuilder.createSymbRef("pop")
                                        )
                                )
                        ),
                        [MathsBuilder.createSymbRefType("rv1")]
                )
        )

        assertNotNull(root)
    }

    @Test
    void testIndividualParameterStandardGeneralWithTransform(){
        def root = ParameterModelBuilder.createStructuredIndividualParameter(
                "idv",
                createStructuredModel(
                        ParameterModelBuilder.createGeneralCovariate(
                                MathsBuilder.createRhs(
                                        MathsBuilder.createBinop(
                                                BinaryOperator.DIVIDE,
                                                MathsBuilder.createSymbRef("cm", "C1"),
                                                MathsBuilder.createSymbRef("pop")
                                        )
                                )
                        ),
                        [MathsBuilder.createSymbRefType("rv1")],
                        ParameterModelBuilder.createParameterTransform(TransformationType.LOGIT)
                )
        )

        assertNotNull(root)
    }

    @Test
    void testIndividualParameterStandardGeneral2Rvs(){
        def root = ParameterModelBuilder.createStructuredIndividualParameter(
                "idv",
                createStructuredModel(
                        ParameterModelBuilder.createGeneralCovariate(
                                MathsBuilder.createRhs(
                                        MathsBuilder.createBinop(
                                                BinaryOperator.DIVIDE,
                                                MathsBuilder.createSymbRef("pop"),
                                                MathsBuilder.createReal(2.9)
                                        )
                                )
                        ),
                        [
                                MathsBuilder.createSymbRefType("rv1"),
                                MathsBuilder.createSymbRefType("rv2")
                        ]
                )
        )

        assertNotNull(root)
    }

    @Test
    void testRvCorrelation(){

        def root = ParameterModelBuilder.createRvCorrelationCorr(
                MathsBuilder.createRhs(MathsBuilder.createSymbRef("p1", "corrVar")),
                MathsBuilder.createSymbRefType("rv1"),
                MathsBuilder.createSymbRefType("rv2")
        )

        assertNotNull(root)
    }

    @Test
    void testRvCovariance(){

        def root = ParameterModelBuilder.createRvCorrelationCov(
                MathsBuilder.createRhs(MathsBuilder.createSymbRef("p1", "covVar")),
                MathsBuilder.createSymbRefType("rv1"),
                MathsBuilder.createSymbRefType("rv2")
        )

        assertNotNull(root)
    }

}
