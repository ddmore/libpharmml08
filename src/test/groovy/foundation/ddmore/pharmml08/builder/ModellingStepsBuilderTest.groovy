/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.commontypes.SymbolTypeType
import foundation.ddmore.pharmml08.dom.modellingsteps.EstimationStepType
import foundation.ddmore.pharmml08.dom.modellingsteps.SimulationStepType
import org.junit.Test

import static foundation.ddmore.pharmml08.builder.ModellingStepsBuilder.*
import static foundation.ddmore.pharmml08.builder.MathsBuilder.*
import static org.junit.Assert.*

class ModellingStepsBuilderTest {

    @Test
    void testModellingStepWithEstimation(){
        def root = createModellingSteps(
                createStepDependencies(),
                createEstimationStep(
                    "testoid",
                    "extDataSet",
                    [
                        createParameterEstimate(
                                createSymbRefType("b", "abc"),
                                createReal(22.0),
                                false,
                                createRhs(createReal(0.0)),
                                createRhs(createReal(30.5))
                        )
                    ],
                    [
                            createOperation(
                                    "op",
                                    1,
                                    "Aname",
                                    createAlgorithm(
                                            "AnAlgo",
                                            [
                                                createProperty("algProp", createRhs(createString("algValue")))
                                            ]
                                    ),
                                    [
                                            createProperty("opProp", createRhs(createString("opValue")))
                                    ]
                            )
                    ]
                )
        )


        assertNotNull(root)

        assertEquals(1, root.getCommonModellingStep().size())
        def estStep = root.commonModellingStep[0].value as EstimationStepType
        assertEquals("testoid", estStep.getOid())
        assertEquals("extDataSet", estStep.getExternalDataSetReference().oidRef.oidRef)
        assertEquals(1, estStep.parametersToEstimate.getParameterEstimation().size())

        def paramToEst = estStep.parametersToEstimate.getParameterEstimation()[0]
        assertEquals("b", paramToEst.symbRef.blkIdRef)
        assertEquals("abc", paramToEst.symbRef.symbIdRef)
        assertEquals(22.0, paramToEst.initialEstimate.expression.value.value, 0.0001)
        assertEquals(0.0, paramToEst.lowerBound.expression.value.value, 0.0001)
        assertEquals(30.5, paramToEst.upperBound.expression.value.value, 0.0001)

        assertEquals(1, estStep.getOperation().size())
        def operation = estStep.getOperation()[0]
        assertEquals("AnAlgo", operation.algorithm.name.value)
        assertEquals("Aname", operation.name.value)
        assertEquals("algProp", operation.algorithm.getProperty()[0].name)
        assertEquals("algValue", operation.algorithm.getProperty()[0].assign.expression.value.value)
        assertEquals("opProp", operation.getProperty()[0].name)
        assertEquals("opValue", operation.getProperty()[0].assign.expression.value.value)
    }

    @Test
    void testModellingStepWithSimulation(){
        def root = createModellingSteps(
                createStepDependencies(),
                createSimulationStep(
                        "testoid",
                        "extDataSet",
                        [
                                createVariableAssignment(
                                        createSymbRefType("b", "abc"),
                                        createRhs(createReal(30.5))
                                )
                        ],
                        [
                                createSimOperation(
                                        "op",
                                        1,
                                        "Aname",
                                        createAlgorithm(
                                                "AnAlgo",
                                                [
                                                        createProperty("algProp", createRhs(createString("algValue")))
                                                ]
                                        ),
                                        [
                                                createProperty("opProp", createRhs(createString("opValue")))
                                        ]
                                )
                        ]
                )
        )


        assertNotNull(root)

        assertEquals(1, root.getCommonModellingStep().size())
        def simStep = root.commonModellingStep[0].value as SimulationStepType
        assertEquals("testoid", simStep.getOid())
        assertEquals("extDataSet", simStep.getExternalDataSetReference().oidRef.oidRef)
        assertEquals(1, simStep.getVariableAssignment().size())

        def varAssign = simStep.getVariableAssignment()[0]
        assertEquals("b", varAssign.symbRef.blkIdRef)
        assertEquals("abc", varAssign.symbRef.symbIdRef)
        assertEquals(30.5, varAssign.assign.expression.value.value, 0.0001)

        assertEquals(1, simStep.getOperation().size())
        def operation = simStep.getOperation()[0]
        assertEquals("AnAlgo", operation.algorithm.name.value)
        assertEquals("Aname", operation.name.value)
        assertEquals("algProp", operation.algorithm.getProperty()[0].name)
        assertEquals("algValue", operation.algorithm.getProperty()[0].assign.expression.value.value)
        assertEquals("opProp", operation.getProperty()[0].name)
        assertEquals("opValue", operation.getProperty()[0].assign.expression.value.value)
    }

    @Test
    void testStepDependencies(){
        def root = createModellingSteps(
                createStepDependencies(
                        createStep("testOid1", "testOid2", "testOid3")
                ),
                createEstimationStep(
                        "testoid1",
                        "extDataSetOid",
                        [
                                createParameterEstimate(
                                        createSymbRefType("p1", "abc"),
                                        createReal(22.0),
                                        false,
                                        createRhs(createReal(0.0)),
                                        createRhs(createReal(30.5))
                                )
                        ],
                        [
                                createOperation(
                                        "op",
                                        1,
                                        "Aname",
                                        createAlgorithm(
                                                "AnAlgo",
                                                [
                                                        createProperty("algProp", createRhs(createString("algValue")))
                                                ]
                                        ),
                                        [
                                                createProperty("opProp", createRhs(createString("opValue")))
                                        ]
                                )
                        ]
                )
        )

        assertNotNull(root)

        def depSteps = root.getStepDependencies()
        assertNotNull(depSteps)

        assertEquals(1, root.getStepDependencies().getStep().size())

        def firstDep = depSteps.getStep()[0]
        assertEquals("testOid1", firstDep.getOidRef().oidRef)

        def expectedDeps = [ "testOid2", "testOid3" ]
        for(int i; i < expectedDeps.size(); i++){
            assertEquals(expectedDeps[i], firstDep.getDependents()[0].oidRef[i].oidRef)
        }
    }

}
