/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import org.junit.Test

import static org.junit.Assert.assertNotNull

/**
 * Created by stumoodie on 05/01/2017.
 */
class CommonVariableBuilderTest {

    @Test
    void testSimpleVariableRhs(){
        def root = StructuralModelBuilder.createVariable(
                "v",
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(0.0)
                )
        )

        assertNotNull(root)
    }


    @Test
    void testDerivativeNoInitTime(){
        def root = StructuralModelBuilder.createDerivative(
                "v",
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(10.0)
                ),
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(22.3)
                ),
                null,
                "T"
        )

        assertNotNull(root)
    }

    @Test
    void testDerivativeInitTimeAndVal(){
        def root = StructuralModelBuilder.createDerivative(
                "v",
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(10.0)
                ),
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(22.3)
                ),
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(-2.3)
                ),
                "T"
        )

        assertNotNull(root)
    }

    @Test
    void testDerivativeNoInitVal(){
        def root = StructuralModelBuilder.createDerivative(
                "v",
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(10.0)
                ),
                null,
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(-2.3)
                ),
                "T"
        )

        assertNotNull(root)
    }

    @Test
    void testDerivativeNoInitValNoIdv(){
        def root = StructuralModelBuilder.createDerivative(
                "v",
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(10.0)
                ),
                null,
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(-2.3)
                )
        )

        assertNotNull(root)
    }

}
