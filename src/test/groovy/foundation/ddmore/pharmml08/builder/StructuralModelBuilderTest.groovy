/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.maths.BinaryOperator
import foundation.ddmore.pharmml08.dom.modeldefn.AbsorptionOralMacroArgumentType
import static StructuralModelBuilder.*

import org.junit.Test

import static org.junit.Assert.assertNotNull

/**
 * Created by stumoodie on 05/01/2017.
 */
class StructuralModelBuilderTest {


    @Test
    void testSingleSimpleDefinition(){
        def root = PharmMlBuilder.createModelDefinition(
                StructuralModelBuilder.createStructuralModel("sm",
                    StructuralModelBuilder.createVariable(
                            "W"
                    )
            )
        )

        assertNotNull(root)
    }


    @Test
    void testMultipleDefinitions(){
        def root = PharmMlBuilder.createModelDefinition(StructuralModelBuilder.createStructuralModel("sm",
                ParameterModelBuilder.createPopulationParam(
                        "pop"
                ),
                StructuralModelBuilder.createVariable("v",
                        MathsBuilder.createRhs(
                                MathsBuilder.createBinop(BinaryOperator.TIMES,
                                        MathsBuilder.createSymbRef("pm", "a"),
                                        MathsBuilder.createSymbRef("b")
                                )
                        )
                ),
                createDerivative("y",
                        MathsBuilder.createRhs(
                                MathsBuilder.createBinop(BinaryOperator.TIMES,
                                        MathsBuilder.createSymbRef("v"),
                                        MathsBuilder.createSymbRef("y")
                                )
                        ),
                        MathsBuilder.createRhs(MathsBuilder.createInt(0)),
                        MathsBuilder.createRhs(MathsBuilder.createInt(0)),
                        "T"
                ),
                createPkMacro(
                        createAbsorptionMacro(
                                [AbsorptionOralMacroArgumentType.TLAG],
                                [MathsBuilder.createRhs(MathsBuilder.createSymbRef("t0"))]
                        )
                )
        )
        )

        assertNotNull(root)
    }

    @Test
    void testMultipleModels(){
        def root = PharmMlBuilder.createModelDefinition(
                 createStructuralModel("sm1",
                        ParameterModelBuilder.createPopulationParam(
                                "W"
                        ),
                ),
                createStructuralModel("sm2",
                        ParameterModelBuilder.createPopulationParam(
                                "Y"
                        ),
                )
        )

        assertNotNull(root)
    }
}
