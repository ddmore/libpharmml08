/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.maths.BinaryOperator
import foundation.ddmore.pharmml08.dom.maths.UnaryOperator
import foundation.ddmore.pharmml08.dom.modeldefn.TransformationType
import foundation.ddmore.pharmml08.dom.probonto.DistroNameType
import foundation.ddmore.pharmml08.dom.probonto.DistroType
import foundation.ddmore.pharmml08.dom.probonto.ParameterNameType
import groovy.transform.TypeChecked
import org.junit.Test

import static foundation.ddmore.pharmml08.builder.MathsBuilder.*
import static foundation.ddmore.pharmml08.builder.ObservationModelBuilder.*
import static foundation.ddmore.pharmml08.builder.PharmMlBuilder.createModelDefinition
import static foundation.ddmore.pharmml08.builder.ProbontoBuilder.*
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotNull

/**
 * Created by stumoodie on 05/01/2017.
 */
class ObservationModelBuilderTest {

    @Test
    void testContinuousObsNoTrans(){
        def root = createObservationModel("om1",
                createContinuousObservation(
                    createStructuredObs("Y",
                            createSymbRefType("sm", "CC"),
                            createRhs(
                                    createFunctionCall(
                                            createSymbRefType("combinedError1"),
                                            createFunctionArgument("additive", createSymbRef("pm", "RUV_ADD"))
                                    )
                            ),
                            createSymbRefType("pm", "EPS_Y")
                    )
            )
        )

        assertNotNull(root)
    }

    @Test
    void testContinuousObsWithTrans(){
        def root = createObservationModel("om1",
                createContinuousObservation(
                    createStructuredObs("Y",
                            createSymbRefType("sm", "CC"),
                            createRhs(
                                    createFunctionCall(createSymbRefType("combinedErrror1"),
                                            createFunctionArgument("additive", createSymbRef("pm", "RUV_ADD"))
                                    )
                            ),
                            createSymbRefType("pm", "EPS_Y"),
                            TransformationType.LOG
                    )
                )
        )

        assertNotNull(root)
    }

    @Test
    void testCountObs(){
        def root = createObservationModel("om1",
                createDiscreteObservation(
                    createCountObs("Y",
                            createDistribution(
                                    createProbonto(DistroNameType.POISSON_1, DistroType.UNIVARIATE,
                                            createDistnParameter(ParameterNameType.RATE,
                                                    createRhs(createSymbRef("sm", "LAMBDA"))
                                            )
                                    )
                            )
                    )
                )
        )
        assertNotNull(root)
    }

    @Test
    void testCategoricalObs(){
        def root = createObservationModel("om1",
                createDiscreteObservation(
                    createCategoricalObs("Y",
                            createDistribution(
                                    createProbonto(DistroNameType.BERNOULLI_1, DistroType.UNIVARIATE,
                                            createDistnParameter(ParameterNameType.PROBABILITY,
                                                    createRhs(createSymbRef("sm", "P1"))
                                            )
                                    )
                            ),
                            "success", "fail"
                    )
                )
        )

        assertNotNull(root)
    }


    @Test
    void testTteObs(){
        def root = createObservationModel("om1",
                createDiscreteObservation(
                        createTTEObs(
                                "Y",
                                "HAZ",
                                createRhs(
                                        createSymbRef("sm", "HAZ")
                                )
                        )
                )
        )
        assertNotNull(root)
    }

}
