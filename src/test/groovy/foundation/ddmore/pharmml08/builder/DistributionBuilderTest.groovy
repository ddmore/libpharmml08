/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.probonto.DistroNameType
import foundation.ddmore.pharmml08.dom.probonto.DistroType
import foundation.ddmore.pharmml08.dom.probonto.ParameterNameType
import org.junit.Test

import static MathsBuilder.createDistribution
import static org.junit.Assert.assertNotNull

/**
 * Created by stumoodie on 05/01/2017.
 */
class DistributionBuilderTest {

    @Test
    void testNormalDistributionWithSd(){
        def root = MathsBuilder.createDistribution(
                UncertMlBuilder.createUncertml(
                        UncertMlBuilder.createNormalDistnSd(
                                UncertMlBuilder.createContinuousValue(22.0),
                                UncertMlBuilder.createPositiveRealValue(33.0)
                        )
                )
        )
        assertNotNull(root)
    }

    @Test
    void testNormalDistributionWithVar(){
        def root = MathsBuilder.createDistribution(
                UncertMlBuilder.createUncertml(
                        UncertMlBuilder.createNormalDistnVar(
                                UncertMlBuilder.createContinuousValue(22.0),
                                UncertMlBuilder.createPositiveRealValue(33.0)
                        )
                )
        )
        assertNotNull(root)
    }

    @Test
    void testNormalDistributionWithVarAndVarRefs(){
        def root = MathsBuilder.createDistribution(
                UncertMlBuilder.createUncertml(
                        UncertMlBuilder.createNormalDistnVar(
                                UncertMlBuilder.createContinuousValue(UncertMlBuilder.createVarRef("mu")),
                                UncertMlBuilder.createPositiveRealValue(UncertMlBuilder.createVarRef("omega"))
                        )
                )
        )
        assertNotNull(root)
    }

    @Test
    void testProbontoNormalDistribution(){
        def root = createDistribution(
                ProbontoBuilder.createProbonto(
                        DistroNameType.NORMAL_1,
                        DistroType.UNIVARIATE,
                        ProbontoBuilder.createDistnParameter(
                                ParameterNameType.MEAN,
                                MathsBuilder.createRhs(
                                        MathsBuilder.createReal(22.0)
                                )

                        ),
                        ProbontoBuilder.createDistnParameter(
                                ParameterNameType.VAR,
                                MathsBuilder.createRhs(
                                        MathsBuilder.createReal(33.0)
                                )
                        )
                )
        )
        assertNotNull(root)
    }

}
