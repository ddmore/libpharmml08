/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08.builder

import foundation.ddmore.pharmml08.dom.commontypes.SymbolTypeType
import foundation.ddmore.pharmml08.dom.commontypes.TrueBooleanType
import foundation.ddmore.pharmml08.dom.dataset.ColumnMappingType
import foundation.ddmore.pharmml08.dom.dataset.ColumnRefType
import foundation.ddmore.pharmml08.dom.dataset.ColumnTypeType
import foundation.ddmore.pharmml08.dom.modellingsteps.EstimationStepType
import foundation.ddmore.pharmml08.dom.trialdesign.MultipleDVMappingType
import org.junit.Test

import static foundation.ddmore.pharmml08.builder.MathsBuilder.*
import static foundation.ddmore.pharmml08.builder.TrialDesignBuilder.*
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotNull
import static org.junit.Assert.assertNull

class TrialDesignBuilderTest {

    @Test
    void testSingleExternalDataset(){
        def root = createTrialDesign(
                createExternalDataset(
                        "tstOid",
                        "NONMEM",
                        createDataset(
                                createExternalFileType("foo/bar"),
                                createColumnDefinition("c1", 1, ColumnTypeType.DOSE, SymbolTypeType.REAL),
                                createColumnDefinition("c2", 2, ColumnTypeType.DV, SymbolTypeType.REAL),
                                createColumnDefinition("c3", 3, ColumnTypeType.CMT, SymbolTypeType.INT)
                        ),
                        createColumnMapping(
                                createColumnRefType("col1"),
                                createSymbRefType("sm", "foo")
                        ),
                        createMultiDvMapping(
                                createColumnRefType("col2"),
                                createPiecewise(
                                        createPiece(
                                                createCondition(
                                                        createBoolean(true)
                                                ),
                                                createString("A")
                                        )
                                ).value
                        ),
                         createColumnMapping(
                                 createColumnRefType("col3"),
                                 createTargetMapping(
                                         "sm",
                                         [
                                                 [ dataSymbol : "1", adm : 1],
                                                 [ dataSymbol : "2", adm : 2]
                                         ]
                                 )
                         ),
                        createColumnMapping(
                                createColumnRefType("col4"),
                                createTargetMapping(
                                        "sm", 1
                                )
                        ),
                        createColumnMapping(
                                createColumnRefType("col5"),
                                createSymbRefType("cm", "Sex"),
                                [
                                        [ dataSymb : "m", modelSymb : "MALE" ],
                                        [ dataSymb : "f", modelSymb : "FEMALE"],
                                        [ dataSymb : "n", modelSymb : "NEUTRAL"]
                                ]
                        )
                )
        )


        assertNotNull(root)

        assertEquals(1, root.getExternalDataSet().size())

        def xtnlDataset = root.getExternalDataSet().head()
        assertEquals(5, xtnlDataset.columnMappingOrColumnTransformationOrMultipleDVMapping.size())

        assertEquals("tstOid", xtnlDataset.oid)
        assertEquals("NONMEM", xtnlDataset.toolName)

        // extnl file
        def xtnlFile = xtnlDataset.dataSet.externalFile
        assertEquals("foo/bar", xtnlFile.path)
        assertEquals("COMMA", xtnlFile.delimiter)
        assertEquals("CSV", xtnlFile.format)

        def expectedColDefns = [
                [
                        id : "c1",
                        pos : 1,
                        colType : ColumnTypeType.DOSE,
                        valType : SymbolTypeType.REAL
                ],
                [
                        id : "c2",
                        pos : 2,
                        colType : ColumnTypeType.DV,
                        valType : SymbolTypeType.REAL
                ],
                [
                        id : "c3",
                        pos : 3,
                        colType : ColumnTypeType.CMT,
                        valType : SymbolTypeType.INT
                ],
        ]

        def colDefns = xtnlDataset.dataSet.definition.column
        assertEquals(expectedColDefns.size(), colDefns.size())
        for(int i = 0; i < expectedColDefns.size(); i++){
            assertEquals(expectedColDefns[i].id, colDefns[i].columnId)
            assertEquals(expectedColDefns[i].pos, colDefns[i].columnNum)
            assertEquals(expectedColDefns[i].colType, colDefns[i].columnType.head())
            assertEquals(expectedColDefns[i].valType, colDefns[i].valueType)
        }

        def mapping1 = xtnlDataset.columnMappingOrColumnTransformationOrMultipleDVMapping[0] as ColumnMappingType
        assertEquals("col1", mapping1.columnRef.columnIdRef)
        assertEquals("sm", mapping1.symbRef.blkIdRef)
        assertEquals("foo", mapping1.symbRef.symbIdRef)

        def mapping2 = xtnlDataset.columnMappingOrColumnTransformationOrMultipleDVMapping[1] as MultipleDVMappingType
        assertEquals("col2", mapping2.columnRef.columnIdRef)
        assertEquals(TrueBooleanType.class, mapping2.piecewise.piece.head().condition.expression.value.class)
        assertEquals("A", mapping2.piecewise.piece.head().expression.value.value)

        def mapping3 = xtnlDataset.columnMappingOrColumnTransformationOrMultipleDVMapping[2] as ColumnMappingType
        assertEquals("col3", mapping3.columnRef.columnIdRef)
        assertEquals(1, mapping3.targetMapping.size())
        assertEquals("sm", mapping3.targetMapping[0].blkIdRef)
        assertEquals(1, mapping3.targetMapping[0].map[0].admNumber)
        assertEquals("1", mapping3.targetMapping[0].map[0].dataSymbol)
        assertEquals(2, mapping3.targetMapping[0].map[1].admNumber)
        assertEquals("2", mapping3.targetMapping[0].map[1].dataSymbol)

        def mapping4 = xtnlDataset.columnMappingOrColumnTransformationOrMultipleDVMapping[3] as ColumnMappingType
        assertEquals("col4", mapping4.columnRef.columnIdRef)
        assertEquals(1, mapping4.targetMapping.size())
        assertEquals("sm", mapping4.targetMapping[0].blkIdRef)
        assertEquals(1, mapping4.targetMapping[0].map[0].admNumber)
        assertNull(mapping4.targetMapping[0].map[0].dataSymbol)

        def mapping5 = xtnlDataset.columnMappingOrColumnTransformationOrMultipleDVMapping[4] as ColumnMappingType
        assertEquals("col5", mapping5.columnRef.columnIdRef)
        assertEquals("cm", mapping5.symbRef.blkIdRef)
        assertEquals("Sex", mapping5.symbRef.symbIdRef)
        assertEquals("MALE", mapping5.categoryMapping[0].map[0].modelSymbol)
        assertEquals("m", mapping5.categoryMapping[0].map[0].dataSymbol)
        assertEquals("FEMALE", mapping5.categoryMapping[0].map[1].modelSymbol)
        assertEquals("f", mapping5.categoryMapping[0].map[1].dataSymbol)
        assertEquals("NEUTRAL", mapping5.categoryMapping[0].map[2].modelSymbol)
        assertEquals("n", mapping5.categoryMapping[0].map[2].dataSymbol)

    }


}
