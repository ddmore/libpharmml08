/*******************************************************************************
 * Copyright (c) 2013 European Molecular Biology Laboratory,
 * Heidelberg, Germany.
 *
 * Copyright (c) 2017 DDMoRe Foundation,
 * Utrecht, NL.
 *
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in
 * compliance with the License.  You may obtain a copy of
 * the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY 
 * KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations 
 * under the License.
 *******************************************************************************/
package foundation.ddmore.pharmml08.impl;

import foundation.ddmore.pharmml08.IErrorHandler;
import foundation.ddmore.pharmml08.TestDomFactory;
import foundation.ddmore.pharmml08.dom.PharmML;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class MarshallerImplValidDomTest {
	private static final String VALID_MODEL_FILE = "examples/example1.xml";
	private MarshallerImpl testInstance;
	private PharmML validDom;
	protected boolean warningOccurred;
	protected boolean errorOccurred;
	

	@Before
	public void setUp() throws Exception {
		this.testInstance = new MarshallerImpl();
		this.warningOccurred = false;
		this.errorOccurred = false;
		validDom = TestDomFactory.createValidModel();
	}

	@After
	public void tearDown() throws Exception {
		this.testInstance = null;
		this.validDom = null;
	}

	@Test
	public void testMarshall() throws IOException {
		File tmpFile = File.createTempFile("tst", "xml");
		OutputStream os = new FileOutputStream(tmpFile);
		this.testInstance.setErrorHandler(new IErrorHandler() {
			@Override
			public void handleWarning(String warnMsg) {
				warningOccurred = true;
			}
			@Override
			public void handleError(String errMsg) {
				errorOccurred = true;
			}
		});
		this.testInstance.marshall(validDom, os);
		assertFalse("No error reported", errorOccurred);
		assertFalse("No warning reported", warningOccurred);
		tmpFile.deleteOnExit();
	}

	@Test
	public void testUnmarshall() throws IOException {
		InputStream is = new FileInputStream(VALID_MODEL_FILE);
		this.testInstance.setErrorHandler(new IErrorHandler() {
			@Override
			public void handleWarning(String warnMsg) {
				warningOccurred = true;
			}
			@Override
			public void handleError(String errMsg) {
				errorOccurred = true;
			}
		});
		PharmML dom = this.testInstance.unmarshall(is);
		assertFalse("No errors reported", errorOccurred);
		assertNotNull("dom created", dom);
	}

}
