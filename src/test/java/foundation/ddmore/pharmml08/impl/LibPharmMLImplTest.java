/*******************************************************************************
 * Copyright (c) 2013 European Molecular Biology Laboratory,
 * Heidelberg, Germany.
 *
 * Copyright (c) 2017 DDMoRe Foundation,
 * Utrecht, NL.
 *
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in
 * compliance with the License.  You may obtain a copy of
 * the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY 
 * KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations 
 * under the License.
 *******************************************************************************/
package foundation.ddmore.pharmml08.impl;

import foundation.ddmore.pharmml08.*;
import foundation.ddmore.pharmml08.dom.PharmML;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import static org.junit.Assert.*;

public class LibPharmMLImplTest {
	private ILibPharmML testInstance;
	private IPharmMLResource validResource;
	private IPharmMLResource invalidResource;
	private IPharmMLValidator validator;

	@Before
	public void setUp() throws Exception {
		this.testInstance = new LibPharmMLImpl();
		this.validator = new PharmMLValidatorImpl();
		this.testInstance.setValidator(validator);
		this.testInstance.setMarshaller(new MarshallerImpl());
		final ValidationReportFactory rptFact = new ValidationReportFactory();
		this.validResource = new IPharmMLResource() {

			@Override
			public PharmML getDom() {
				return TestDomFactory.createValidModel();
			}

			@Override
			public IValidationReport getCreationReport() {
				return rptFact.createReport();
			}
		};
		this.invalidResource = new IPharmMLResource() {

			@Override
			public PharmML getDom() {
				return TestDomFactory.createInvalidModel();
			}

			@Override
			public IValidationReport getCreationReport() {
				return rptFact.createReport();
			}
		};
	}

	@After
	public void tearDown() throws Exception {
		this.testInstance = null;
		this.validResource = null;
	}

	@Test
	public void testSave() throws IOException, SAXException, ParserConfigurationException {
		File tmpFile = File.createTempFile("tst", "xml");
		tmpFile.deleteOnExit();
		OutputStream os = new FileOutputStream(tmpFile);
		this.testInstance.save(os, validResource);
		os.close();
		assertTrue("Valid doc", checkFileIsValid(tmpFile));
	}

	@Test
	public void testInvalidSave() throws IOException, SAXException, ParserConfigurationException {
		File tmpFile = File.createTempFile("tst", "xml");
		tmpFile.deleteOnExit();
		OutputStream os = new FileOutputStream(tmpFile);
		this.testInstance.save(os, invalidResource);
		os.close();
		assertFalse("Invalid doc", checkFileIsValid(tmpFile));
	}

	private boolean checkFileIsValid(File tmpFile) throws IOException, SAXException, ParserConfigurationException {
		Schema s = PharmMLSchemaFactory.getInstance().createPharmMlSchema();
		Validator v = s.newValidator();
		StreamSource instanceDocument = new StreamSource(tmpFile);
		ValidationErrorHander validnHandler = new ValidationErrorHander();
		v.setErrorHandler(validnHandler);
		v.validate(instanceDocument);
		return validnHandler.isValid();
	}

	@Test
	public void testCreateDomFromResource() {
		IPharmMLResource res = this.testInstance.createDom();
		assertNotNull("exists", res);
		assertNotNull("dom exists", res.getDom());
		assertNotNull("rept exists", res.getCreationReport());
		assertNotNull("valid rpt", res.getCreationReport().isValid());
		// the above may be hard coded so we'll check to make sure.
		IValidationReport rpt = this.validator.createValidationReport(res);
		assertTrue("stub dom is valid", rpt.isValid());
	}

	@Test
	public void testCreateDom() {
		IPharmMLResource res = this.testInstance.createDom();
		assertNotNull("exists", res);
		assertNotNull("dom exists", res.getDom());
		assertNotNull("rept exists", res.getCreationReport());
		assertNotNull("valid rpt", res.getCreationReport().isValid());
		// the above may be hard coded so we'll check to make sure.
		IValidationReport rpt = this.validator.createValidationReport(res);
		assertTrue("stub dom is valid", rpt.isValid());
	}

	public class ValidationErrorHander implements ErrorHandler {
		private boolean isValid = true;

		@Override
		public void warning(SAXParseException exception) throws SAXException {
		}

		@Override
		public void error(SAXParseException exception) throws SAXException {
			this.isValid = false;
		}

		@Override
		public void fatalError(SAXParseException exception) throws SAXException {
			this.isValid = false;
		}

		
		public boolean isValid(){
			return this.isValid;
		}
	}
}
