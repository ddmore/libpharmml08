/*
 *  Copyright (c) 2018.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml08;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import static org.junit.Assert.*;

/**
 * Tests integration with libPharmML
 */
public class LibPharmMLIntegrationTest {

    private static final Logger LOG = LoggerFactory.getLogger(LibPharmMLIntegrationTest.class);

    @Test
    public void shouldValidatePharmMLResource() throws IOException {
        ILibPharmML libPharmML = PharmMlFactory.getInstance().createLibPharmML();

        String modelFile = "UseCase1.xml";
        try(InputStream in = this.getClass().getResourceAsStream(modelFile)){
            IPharmMLResource res = libPharmML.createDomFromResource(in);
            Iterator<IValidationError> iter = res.getCreationReport().errorIterator();
            while(iter.hasNext()){
                IValidationError err = iter.next();
                LOG.debug("Validation error {}: {}", err.getRuleId(), err.getErrorMsg());
            }
            assertTrue("expect valid", res.getCreationReport().isValid());
            assertEquals(0, res.getCreationReport().numErrors());
        }
    }

    @Test
    public void shouldInValidatePharmMLResource() throws IOException {
        ILibPharmML libPharmML = PharmMlFactory.getInstance().createLibPharmML();
        String modelFile = "InvalidUseCase1.xml";
        try(InputStream in = this.getClass().getResourceAsStream(modelFile)){
            IPharmMLResource res = libPharmML.createDomFromResource(in);
            assertFalse("expect invalid", res.getCreationReport().isValid());
            assertEquals(2, res.getCreationReport().numErrors());
        }
    }
}
