/*******************************************************************************
 * Copyright (c) 2013 European Molecular Biology Laboratory,
 * Heidelberg, Germany.
 *
 * Copyright (c) 2017 DDMoRe Foundation,
 * Utrecht, NL.

 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in
 * compliance with the License.  You may obtain a copy of
 * the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY 
 * KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations 
 * under the License.
 *******************************************************************************/
package foundation.ddmore.pharmml08;

import foundation.ddmore.pharmml08.dom.PharmML;
import foundation.ddmore.pharmml08.dom.modeldefn.ModelDefinitionType;
import foundation.ddmore.pharmml08.dom.modeldefn.StructuralModelType;
import foundation.ddmore.pharmml08.impl.Messages;
import foundation.ddmore.pharmml08.impl.PharmMLSchemaFactory;
import foundation.ddmore.pharmml08.dom.ObjectFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class TestDomFactory {
	private static final String CONTEXT_NAME = Messages.getString("MarshallerImpl.contextDefn"); //$NON-NLS-1$

	public static PharmML createInvalidModel(){
		ObjectFactory fact = new ObjectFactory();
		PharmML dom = fact.createPharmML();
		foundation.ddmore.pharmml08.dom.modeldefn.ObjectFactory mdefnFact = new foundation.ddmore.pharmml08.dom.modeldefn.ObjectFactory();
		ModelDefinitionType mdt = mdefnFact.createModelDefinitionType();
		StructuralModelType stm = mdefnFact.createStructuralModelType();
		stm.setBlkId("main");
		mdt.getStructuralModel().add(stm);
		mdefnFact.createModelDefinition(mdt);
		dom.setModelDefinition(mdt);
		return dom;
	}
	
	public static PharmML createValidModel(){
		try {
			Schema mySchema = PharmMLSchemaFactory.getInstance().createPharmMlSchema();
		InputStream is = new FileInputStream(new File("examples/example1.xml"));
//		String packageName = PharmML.class.getPackage().getName();
		JAXBContext context = JAXBContext.newInstance(CONTEXT_NAME);
		Unmarshaller u = context.createUnmarshaller();
		u.setSchema(mySchema);
		PharmML doc = (PharmML)u.unmarshal(is);
		return doc;
		} catch (IOException | JAXBException e) {
			throw new RuntimeException(e);
		}
	}
	
}
