/*******************************************************************************
 * Copyright (c) 2013 European Molecular Biology Laboratory,
 * Heidelberg, Germany.
 *
 * Copyright (c) 2017 DDMoRe Foundation,
 * Utrecht, NL.

 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in
 * compliance with the License.  You may obtain a copy of
 * the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY 
 * KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations 
 * under the License.
 *******************************************************************************/
package foundation.ddmore.pharmml08;

import foundation.ddmore.pharmml08.dom.PharmML;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class SchemaValidityRegressionTest {
	private static final String VALID_MODEL_FILE = "validPharmML";
    private static final String INVALID_MODEL_FILE = "invalidPharmML";
	private ILibPharmML testInstance;
//	private IPharmMLResource testResource;
	
	@Before
	public void setUp() throws Exception {
		this.testInstance = PharmMlFactory.getInstance().createLibPharmML();
//		this.testResource = createValidResource();
	}

	@After
	public void tearDown() throws Exception {
		this.testInstance = null;
//		this.testResource = null;
	}

//	private IPharmMLResource createValidResource() throws IOException{
//		final PharmML dom = TestDomFactory.createValidModel();
//		final List<IValidationError> errList = Collections.emptyList();
//		IPharmMLResource res = new IPharmMLResource() {
//			@Override
//			public PharmML getDom() {
//				return dom;
//			}
//			@Override
//			public IValidationReport getCreationReport() {
//				return new IValidationReport() {
//					@Override
//					public int numErrors() {
//						return errList.size();
//					}
//					@Override
//					public boolean isValid() {
//						return errList.isEmpty();
//					}
//					@Override
//					public IValidationError getError(int errNum) {
//						return errList.get(errNum-1);
//					}
//					@Override
//					public Iterator<IValidationError> errorIterator() {
//						return errList.iterator();
//					}
//				};
//			}
//		};
//		return res;
//	}

	@Parameterized.Parameter(0)
    public String fileName;

    @Parameterized.Parameter(1)
    public File inputXMLFile;

    @Parameterized.Parameter(2)
    public Boolean expectValid;

    @Parameterized.Parameters(name= "{index}: Model {0}")
    public static Collection<Object[]> getModelsToTest() throws Exception {
        ArrayList<Object[]> retVal = new ArrayList<Object[]>();
        retVal.addAll(paramsFromFilePath(VALID_MODEL_FILE, Boolean.TRUE));
        retVal.addAll(paramsFromFilePath(INVALID_MODEL_FILE, Boolean.FALSE));
        return retVal;
    }

    private static List<Object[]> paramsFromFilePath(String dirName, Boolean isValid) throws URISyntaxException {
        ArrayList<Object[]> retVal = new ArrayList<Object[]>();
        URL url = SchemaValidityRegressionTest.class.getClassLoader().getResource(dirName);
        File filesDirFile = new File(url.toURI());
        for(File f : filesDirFile.listFiles()){
            retVal.add(new Object[]{f.getName(), f, isValid});
        }
        return retVal;
    }


	@Test
	public void testLoadValidModel() throws IOException, URISyntaxException {
        try(InputStream in = new BufferedInputStream(new FileInputStream(inputXMLFile))) {
            IPharmMLResource res = this.testInstance.createDomFromResource(in);
            assertEquals("Valid model", expectValid, res.getCreationReport().isValid());
        }
	}

}
