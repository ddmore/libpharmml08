/*******************************************************************************
 * Copyright (c) 2013 European Molecular Biology Laboratory,
 * Heidelberg, Germany.
 *
 * Copyright (c) 2017 DDMoRe Foundation,
 * Utrecht, NL.

 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in
 * compliance with the License.  You may obtain a copy of
 * the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY 
 * KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations 
 * under the License.
 *******************************************************************************/
package foundation.ddmore.pharmml08;

import foundation.ddmore.pharmml08.dom.PharmML;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LibPharmMLValidIntegrationTest {
    private final static Logger LOGGER = LoggerFactory.getLogger(LibPharmMLValidIntegrationTest.class);
	private static final String VALID_MODEL_FILE = "/example4.xml";
	private static final String VALID_MDL_NAME = "Example 4 - estimation with IOV1 (TrialDesign)";
	private ILibPharmML testInstance;
	private IPharmMLResource testResource;
	
	@Before
	public void setUp() throws Exception {
		this.testInstance = PharmMlFactory.getInstance().createLibPharmML();
		this.testResource = createValidResource();
	}

	@After
	public void tearDown() throws Exception {
		this.testInstance = null;
		this.testResource = null;
	}

	private IPharmMLResource createValidResource() throws IOException{
		final PharmML dom = TestDomFactory.createValidModel();
		final List<IValidationError> errList = Collections.emptyList();
		IPharmMLResource res = new IPharmMLResource() {
			@Override
			public PharmML getDom() {
				return dom;
			}
			@Override
			public IValidationReport getCreationReport() {
				return new IValidationReport() {
					@Override
					public int numErrors() {
						return errList.size();
					}
					@Override
					public boolean isValid() {
						return errList.isEmpty();
					}
					@Override
					public IValidationError getError(int errNum) {
						return errList.get(errNum-1);
					}
					@Override
					public Iterator<IValidationError> errorIterator() {
						return errList.iterator();
					}
				};
			}
		};
		return res;
	}
	
	
	@Test
	public void testLoadValidModel() throws IOException {
		InputStream in = this.getClass().getResourceAsStream(VALID_MODEL_FILE);
		IPharmMLResource res = this.testInstance.createDomFromResource(in);
		in.close();
		assertEquals("expected model", VALID_MDL_NAME, res.getDom().getName().getValue());
		assertTrue("Valid model", res.getCreationReport().isValid());
		assertEquals("Valid model", 0, res.getCreationReport().numErrors());
	}

	@Test
	public void testLoadModel() throws IOException {
		InputStream in = this.getClass().getResourceAsStream(VALID_MODEL_FILE);
		IPharmMLResource res = this.testInstance.createDomFromResource(in);
		PharmML dom = res.getDom();
		in.close();
		assertEquals("expected model", VALID_MDL_NAME, dom.getName().getValue());
		assertEquals("0.8.1", dom.getWrittenVersion());
		assertEquals("Example 4 - estimation with IOV1 and with covariates (TrialDesign)", dom.getDescription().getValue());
	}

    @Test
	public void testValidateValidModel() throws IOException{
		IPharmMLValidator validator = this.testInstance.getValidator();
		IValidationReport rpt = validator.createValidationReport(testResource);
		assertTrue("Valid model", rpt.isValid());
		assertEquals("Valid model", 0, rpt.numErrors());
	}
	
	@Test
	public void testSaveValidModel() throws IOException{
		File tmpFile = File.createTempFile("test", ".xml");
		LOGGER.debug("Writing to tmp file: " + tmpFile.getAbsolutePath());
//        File tmpFile = new File("test.xml");
//		tmpFile.deleteOnExit();
		OutputStream os = new FileOutputStream(tmpFile);
		this.testInstance.save(os, testResource);
		os.close();

        InputStream in = new FileInputStream(tmpFile);
        IPharmMLResource res = this.testInstance.createDomFromResource(in);
        in.close();
        IValidationReport rep = this.testInstance.getValidator().createValidationReport(res);
        assertTrue(rep.isValid());

		tmpFile.deleteOnExit();
	}
}
