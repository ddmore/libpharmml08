<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
  ~
  ~  Licensed under the Apache License, Version 2.0 (the
  ~  "License"); you may not use this file except in
  ~  compliance with the License.  You may obtain a copy of
  ~  the License at
  ~
  ~       http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~  Unless required by applicable law or agreed to in writing,
  ~  software distributed under the License is distributed on
  ~  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~  KIND, either express or implied. See the License for the
  ~  specific language governing permissions and limitations
  ~  under the License.
  ~
  -->

<PharmML
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns="http://www.pharmml.org/pharmml/0.8/PharmML"
	xsi:schemaLocation="http://www.pharmml.org/pharmml/0.8/PharmML http://www.pharmml.org/pharmml/0.8/PharmML"
	xmlns:math="http://www.pharmml.org/pharmml/0.8/Maths"
	xmlns:ct="http://www.pharmml.org/pharmml/0.8/CommonTypes"
	xmlns:ds="http://www.pharmml.org/pharmml/0.8/Dataset"
	xmlns:mdef="http://www.pharmml.org/pharmml/0.8/ModelDefinition"
	xmlns:mstep="http://www.pharmml.org/pharmml/0.8/ModellingSteps"
	xmlns:design="http://www.pharmml.org/pharmml/0.8/TrialDesign"
	writtenVersion="0.8.1">
	<ct:Name>Generated from MDL. MOG ID: warfarin_PK_ODE_mog</ct:Name>
	<IndependentVariable symbId="T"/>
	<ct:FunctionDefinition xmlns="http://www.pharmml.org/pharmml/0.8/CommonTypes"
		symbId="combinedError1"
		symbolType="real">
	    <Description>Combined additive ad proportional for 1 epsilon</Description>
	    <FunctionArgument symbolType="real" symbId="additive"/>
	    <FunctionArgument symbolType="real" symbId="proportional"/>
	    <FunctionArgument symbolType="real" symbId="f"/>
	    <Definition>
	        <Assign>
	            <Binop op="plus" xmlns="http://www.pharmml.org/pharmml/0.8/Maths">
	                <ct:SymbRef symbIdRef="additive"/>
	                <Binop op="times">
	                    <ct:SymbRef symbIdRef="proportional"/>
	                    <ct:SymbRef symbIdRef="f"/>
	                </Binop>
	            </Binop>
	        </Assign>
	    </Definition>
	</ct:FunctionDefinition>
	<ModelDefinition xmlns="http://www.pharmml.org/pharmml/0.8/ModelDefinition">
		<VariabilityModel blkId="vm_err" type="residualError">
			<Level symbId="DV"/>
		</VariabilityModel>
		<VariabilityModel blkId="vm_mdl" type="parameterVariability">
			<Level symbId="ID"/>
		</VariabilityModel>
		<CovariateModel blkId="cm">
			<Covariate symbId="logtWT">
				<Continuous/>
			</Covariate>
		</CovariateModel>
		<ParameterModel blkId="pm">
			<PopulationParameter symbId = "POP_CL"/>
			<PopulationParameter symbId = "POP_V"/>
			<PopulationParameter symbId = "POP_KA"/>
			<PopulationParameter symbId = "POP_TLAG"/>
			<PopulationParameter symbId = "BETA_CL_WT"/>
			<PopulationParameter symbId = "BETA_V_WT"/>
			<PopulationParameter symbId = "PPV_CL_V_KA"/>
			<PopulationParameter symbId = "DF_CL_V_KA"/>
			<PopulationParameter symbId = "PPV_TLAG"/>
			<PopulationParameter symbId = "RUV_PROP"/>
			<PopulationParameter symbId = "RUV_ADD"/>
			<RandomVariable symbId="ETA_CL_V_KA">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="MultivariateStudentT1">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Vector>
									<ct:VectorElements>
										<ct:Int>0</ct:Int>
										<ct:Int>0</ct:Int>
										<ct:Int>0</ct:Int>
									</ct:VectorElements>
								</ct:Vector>
							</ct:Assign>
						</Parameter>
						<Parameter name="covarianceMatrix">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="PPV_CL_V_KA"/>
							</ct:Assign>
						</Parameter>
						<Parameter name="degreesOfFreedom">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="DF_CL_V_KA"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
			<RandomVariable symbId="ETA_TLAG">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal2">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Int>0</ct:Int>
							</ct:Assign>
						</Parameter>
						<Parameter name="var">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="PPV_TLAG"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
			<PopulationParameter symbId="MDL__ETA_CL_V_KA_1">
				<ct:Assign>
					<ct:VectorSelector>
						<ct:SymbRef blkIdRef="pm" symbIdRef="ETA_CL_V_KA"/>
						<ct:Cell>
							<ct:Assign>
								<ct:Int>1</ct:Int>
							</ct:Assign>
						</ct:Cell>
					</ct:VectorSelector>
				</ct:Assign>
			</PopulationParameter>
			<IndividualParameter symbId="CL">
				<StructuredModel>
					<Transformation type="log" />
					<LinearCovariate>
						<PopulationValue>
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="POP_CL"/>
							</ct:Assign>
						</PopulationValue>
						<Covariate>
							<ct:SymbRef blkIdRef="cm" symbIdRef="logtWT"/>
							<FixedEffect>
								<ct:SymbRef blkIdRef="pm" symbIdRef="BETA_CL_WT"/>
							</FixedEffect>
						</Covariate>
					</LinearCovariate>
					<RandomEffects>
						<ct:SymbRef blkIdRef="pm" symbIdRef="MDL__ETA_CL_V_KA_1"/>
					</RandomEffects>
				</StructuredModel>
			</IndividualParameter>
			<PopulationParameter symbId="MDL__ETA_CL_V_KA_2">
				<ct:Assign>
					<ct:VectorSelector>
						<ct:SymbRef blkIdRef="pm" symbIdRef="ETA_CL_V_KA"/>
						<ct:Cell>
							<ct:Assign>
								<ct:Int>2</ct:Int>
							</ct:Assign>
						</ct:Cell>
					</ct:VectorSelector>
				</ct:Assign>
			</PopulationParameter>
			<IndividualParameter symbId="V">
				<StructuredModel>
					<Transformation type="log" />
					<LinearCovariate>
						<PopulationValue>
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="POP_V"/>
							</ct:Assign>
						</PopulationValue>
						<Covariate>
							<ct:SymbRef blkIdRef="cm" symbIdRef="logtWT"/>
							<FixedEffect>
								<ct:SymbRef blkIdRef="pm" symbIdRef="BETA_V_WT"/>
							</FixedEffect>
						</Covariate>
					</LinearCovariate>
					<RandomEffects>
						<ct:SymbRef blkIdRef="pm" symbIdRef="MDL__ETA_CL_V_KA_2"/>
					</RandomEffects>
				</StructuredModel>
			</IndividualParameter>
			<PopulationParameter symbId="MDL__ETA_CL_V_KA_3">
				<ct:Assign>
					<ct:VectorSelector>
						<ct:SymbRef blkIdRef="pm" symbIdRef="ETA_CL_V_KA"/>
						<ct:Cell>
							<ct:Assign>
								<ct:Int>3</ct:Int>
							</ct:Assign>
						</ct:Cell>
					</ct:VectorSelector>
				</ct:Assign>
			</PopulationParameter>
			<IndividualParameter symbId="KA">
				<StructuredModel>
					<Transformation type="log" />
					<LinearCovariate>
						<PopulationValue>
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="POP_KA"/>
							</ct:Assign>
						</PopulationValue>
					</LinearCovariate>
					<RandomEffects>
						<ct:SymbRef blkIdRef="pm" symbIdRef="MDL__ETA_CL_V_KA_3"/>
					</RandomEffects>
				</StructuredModel>
			</IndividualParameter>
			<IndividualParameter symbId="TLAG">
				<StructuredModel>
					<Transformation type="log" />
					<LinearCovariate>
						<PopulationValue>
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="POP_TLAG"/>
							</ct:Assign>
						</PopulationValue>
					</LinearCovariate>
					<RandomEffects>
						<ct:SymbRef blkIdRef="pm" symbIdRef="ETA_TLAG"/>
					</RandomEffects>
				</StructuredModel>
			</IndividualParameter>
			<RandomVariable symbId="EPS_Y">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_err" symbIdRef="DV"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal2">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Int>0</ct:Int>
							</ct:Assign>
						</Parameter>
						<Parameter name="var">
							<ct:Assign>
								<ct:Int>1</ct:Int>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
		</ParameterModel>
		<StructuralModel blkId="sm">
			<ct:Variable symbId="CC" symbolType="real">
				<ct:Assign>
					<math:Binop op="divide">
						<ct:SymbRef blkIdRef="sm" symbIdRef="CENTRAL"/>
						<ct:SymbRef blkIdRef="pm" symbIdRef="V"/>
					</math:Binop>
				</ct:Assign>
			</ct:Variable>
			<ct:Variable symbId="CENTRAL" symbolType="real"/>
			<PKmacros>
				<Absorption>
					<Value argument="adm"> 
						<ct:Int>1</ct:Int>
					</Value>
					<Value argument="cmt"> 
						<ct:Int>2</ct:Int>
					</Value>
					<Value argument="ka"> 
						<ct:Assign>
							<ct:SymbRef blkIdRef="pm" symbIdRef="KA"/>
						</ct:Assign>
					</Value>
					<Value argument="Tlag"> 
						<ct:Assign>
							<ct:SymbRef blkIdRef="pm" symbIdRef="TLAG"/>
						</ct:Assign>
					</Value>
				</Absorption>
				<Compartment>
					<Value argument="amount"> 
						<ct:SymbRef blkIdRef="sm" symbIdRef="CENTRAL"/>
					</Value>
					<Value argument="cmt"> 
						<ct:Int>2</ct:Int>
					</Value>
				</Compartment>
				<Elimination>
					<Value argument="cmt"> 
						<ct:Int>2</ct:Int>
					</Value>
					<Value argument="V"> 
						<ct:Assign>
							<ct:SymbRef blkIdRef="pm" symbIdRef="V"/>
						</ct:Assign>
					</Value>
					<Value argument="CL"> 
						<ct:Assign>
							<ct:SymbRef blkIdRef="pm" symbIdRef="CL"/>
						</ct:Assign>
					</Value>
				</Elimination>
			</PKmacros>
		</StructuralModel>
		<ObservationModel blkId="om1">
			<ContinuousData>
				<Standard symbId="Y">
					<Output>
						<ct:SymbRef blkIdRef="sm" symbIdRef="CC"/>
					</Output>
					<ErrorModel>
						<ct:Assign>
							<math:FunctionCall>
								<ct:SymbRef symbIdRef="combinedError1"/>
								<math:FunctionArgument symbId="additive">
									<ct:SymbRef blkIdRef="pm" symbIdRef="RUV_ADD"/>
								</math:FunctionArgument>
								<math:FunctionArgument symbId="proportional">
									<ct:SymbRef blkIdRef="pm" symbIdRef="RUV_PROP"/>
								</math:FunctionArgument>
								<math:FunctionArgument symbId="f">
									<ct:SymbRef blkIdRef="sm" symbIdRef="CC"/>
								</math:FunctionArgument>
							</math:FunctionCall>
						</ct:Assign>
					</ErrorModel>
					<ResidualError>
						<ct:SymbRef blkIdRef="pm" symbIdRef="EPS_Y"/>
					</ResidualError>
				</Standard>
			</ContinuousData>
		</ObservationModel>
	</ModelDefinition>
	<TrialDesign xmlns="http://www.pharmml.org/pharmml/0.8/TrialDesign">
		<ExternalDataSet toolName="Monolix" oid="nm_ds">
			<ColumnMapping>
				<ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="ID"/>
				<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
			</ColumnMapping>
			<ColumnMapping>
				<ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="TIME"/>
				<ct:SymbRef symbIdRef="T"/>
			</ColumnMapping>
			<ColumnMapping>
				<ds:ColumnRef columnIdRef="AMT"/>
				<ds:TargetMapping blkIdRef="sm">
					<ds:Map admNumber="1"/>
				</ds:TargetMapping>
			</ColumnMapping>
			<ColumnMapping>
			    <ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="DV"/>
				<ct:SymbRef blkIdRef="om1" symbIdRef="Y"/>
			</ColumnMapping>
			<ColumnMapping>
				<ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="logtWT"/>
				<ct:SymbRef blkIdRef="cm" symbIdRef="logtWT"/>
			</ColumnMapping>
			<DataSet xmlns="http://www.pharmml.org/pharmml/0.8/Dataset">
				<Definition>
					<Column columnId="ID" columnType="id" valueType="int" columnNum="1"/>
					<Column columnId="TIME" columnType="idv" valueType="real" columnNum="2"/>
					<Column columnId="WT" columnType="undefined" valueType="real" columnNum="3"/>
					<Column columnId="AMT" columnType="dose" valueType="real" columnNum="4"/>
					<Column columnId="DVID" columnType="dvid" valueType="int" columnNum="5"/>
					<Column columnId="DV" columnType="dv" valueType="real" columnNum="6"/>
					<Column columnId="MDV" columnType="mdv" valueType="int" columnNum="7"/>
					<Column columnId="logtWT" columnType="covariate" valueType="real" columnNum="8"/>
				</Definition>
				<ExternalFile oid="id">
					<path>warfarin_conc.csv</path>
					<format>CSV</format>
					<delimiter>COMMA</delimiter>
				</ExternalFile>
			</DataSet>
		</ExternalDataSet>
	</TrialDesign>
	<ModellingSteps xmlns="http://www.pharmml.org/pharmml/0.8/ModellingSteps">
		<EstimationStep oid="estimStep_1">
			<ExternalDataSetReference>
				<ct:OidRef oidRef="nm_ds"/>
			</ExternalDataSetReference>
			<ParametersToEstimate>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="POP_CL"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.1</ct:Real>
					</InitialEstimate>
					<LowerBound>
						<ct:Real>0.001</ct:Real>
					</LowerBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="POP_V"/>
					<InitialEstimate fixed="false">
						<ct:Int>8</ct:Int>
					</InitialEstimate>
					<LowerBound>
						<ct:Real>0.001</ct:Real>
					</LowerBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="POP_KA"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.362</ct:Real>
					</InitialEstimate>
					<LowerBound>
						<ct:Real>0.001</ct:Real>
					</LowerBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="POP_TLAG"/>
					<InitialEstimate fixed="false">
						<ct:Int>1</ct:Int>
					</InitialEstimate>
					<LowerBound>
						<ct:Real>0.001</ct:Real>
					</LowerBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="BETA_CL_WT"/>
					<InitialEstimate fixed="true">
						<ct:Real>0.75</ct:Real>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="BETA_V_WT"/>
					<InitialEstimate fixed="true">
						<ct:Int>1</ct:Int>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="PPV_CL_V_KA"/>
					<InitialEstimate fixed="false">
						<ct:Matrix matrixType="LowerTriangular">
							<ct:MatrixRow>
								<ct:Real>0</ct:Real>
							</ct:MatrixRow>
							<ct:MatrixRow>
								<ct:Real>0.01</ct:Real>
								<ct:Real>0</ct:Real>
							</ct:MatrixRow>
							<ct:MatrixRow>
								<ct:Real>0.01</ct:Real>
								<ct:Real>0.01</ct:Real>
								<ct:Real>0</ct:Real>
							</ct:MatrixRow>
						</ct:Matrix>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="DF_CL_V_KA"/>
					<InitialEstimate fixed="true">
						<ct:Int>3</ct:Int>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="PPV_TLAG"/>
					<InitialEstimate fixed="true">
						<ct:Real>0.1</ct:Real>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="RUV_PROP"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.1</ct:Real>
					</InitialEstimate>
					<LowerBound>
						<ct:Int>0</ct:Int>
					</LowerBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="RUV_ADD"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.1</ct:Real>
					</InitialEstimate>
					<LowerBound>
						<ct:Int>0</ct:Int>
					</LowerBound>
				</ParameterEstimation>
			</ParametersToEstimate>
			<Operation order="1" opType="generic">
				<Property name="algo">
					<ct:Assign>
						<ct:String>saem</ct:String>
					</ct:Assign>
				</Property>
			</Operation>
		</EstimationStep>
		<mstep:StepDependencies>
			<mstep:Step>
				<ct:OidRef oidRef="estimStep_1"/>
			</mstep:Step>
		</mstep:StepDependencies>
	</ModellingSteps>		
</PharmML>
