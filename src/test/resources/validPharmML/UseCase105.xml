<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
  ~
  ~  Licensed under the Apache License, Version 2.0 (the
  ~  "License"); you may not use this file except in
  ~  compliance with the License.  You may obtain a copy of
  ~  the License at
  ~
  ~       http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~  Unless required by applicable law or agreed to in writing,
  ~  software distributed under the License is distributed on
  ~  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~  KIND, either express or implied. See the License for the
  ~  specific language governing permissions and limitations
  ~  under the License.
  ~
  -->

<PharmML
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns="http://www.pharmml.org/pharmml/0.8/PharmML"
	xsi:schemaLocation="http://www.pharmml.org/pharmml/0.8/PharmML http://www.pharmml.org/pharmml/0.8/PharmML"
	xmlns:math="http://www.pharmml.org/pharmml/0.8/Maths"
	xmlns:ct="http://www.pharmml.org/pharmml/0.8/CommonTypes"
	xmlns:ds="http://www.pharmml.org/pharmml/0.8/Dataset"
	xmlns:mdef="http://www.pharmml.org/pharmml/0.8/ModelDefinition"
	xmlns:mstep="http://www.pharmml.org/pharmml/0.8/ModellingSteps"
	xmlns:design="http://www.pharmml.org/pharmml/0.8/TrialDesign"
	writtenVersion="0.8.1">
	<ct:Name>Generated from MDL. MOG ID: drugX_delAbs_1occ_v1_mog</ct:Name>
	<IndependentVariable symbId="T"/>
	<ModelDefinition xmlns="http://www.pharmml.org/pharmml/0.8/ModelDefinition">
		<VariabilityModel blkId="vm_err" type="residualError">
			<Level symbId="DV"/>
		</VariabilityModel>
		<VariabilityModel blkId="vm_mdl" type="parameterVariability">
			<Level symbId="ID"/>
		</VariabilityModel>
		<CovariateModel blkId="cm">
			<Covariate symbId="WT">
				<Continuous/>
			</Covariate>
			<Covariate symbId="AMT">
				<Continuous/>
			</Covariate>
			<Covariate symbId="RATE">
				<Continuous/>
			</Covariate>
		</CovariateModel>
		<ParameterModel blkId="pm">
			<PopulationParameter symbId = "pop_Vc"/>
			<PopulationParameter symbId = "pop_Vp"/>
			<PopulationParameter symbId = "pop_CL"/>
			<PopulationParameter symbId = "pop_Ka"/>
			<PopulationParameter symbId = "pop_Q"/>
			<PopulationParameter symbId = "pop_MTT"/>
			<PopulationParameter symbId = "pop_n"/>
			<PopulationParameter symbId = "b"/>
			<PopulationParameter symbId = "omega_Vc"/>
			<PopulationParameter symbId = "omega_Vp"/>
			<PopulationParameter symbId = "omega_CL"/>
			<PopulationParameter symbId = "sigma"/>
			<PopulationParameter symbId = "grp_Vc">
				<ct:Assign>
					<math:Binop op="times">
						<ct:SymbRef blkIdRef="pm" symbIdRef="pop_Vc"/>
						<math:Binop op="divide">
							<ct:SymbRef blkIdRef="cm" symbIdRef="WT"/>
							<ct:Int>70</ct:Int>
						</math:Binop>
					</math:Binop>
				</ct:Assign>
			</PopulationParameter>
			<RandomVariable symbId="eta_Vc">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal2">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Int>0</ct:Int>
							</ct:Assign>
						</Parameter>
						<Parameter name="var">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="omega_Vc"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
			<RandomVariable symbId="eta_Vp">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal2">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Int>0</ct:Int>
							</ct:Assign>
						</Parameter>
						<Parameter name="var">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="omega_Vp"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
			<RandomVariable symbId="eta_CL">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal2">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Int>0</ct:Int>
							</ct:Assign>
						</Parameter>
						<Parameter name="var">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="omega_CL"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
			<RandomVariable symbId="epsilon_Cc">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_err" symbIdRef="DV"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal2">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Int>0</ct:Int>
							</ct:Assign>
						</Parameter>
						<Parameter name="var">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="sigma"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
			<IndividualParameter symbId="Vc">
				<ct:Assign>
					<math:Binop op="times">
						<ct:SymbRef blkIdRef="pm" symbIdRef="grp_Vc"/>
						<math:Uniop op="exp">
							<ct:SymbRef blkIdRef="pm" symbIdRef="eta_Vc"/>
						</math:Uniop>	
					</math:Binop>
				</ct:Assign>
			</IndividualParameter>
			<IndividualParameter symbId="Vp">
				<ct:Assign>
					<math:Binop op="times">
						<ct:SymbRef blkIdRef="pm" symbIdRef="pop_Vp"/>
						<math:Uniop op="exp">
							<ct:SymbRef blkIdRef="pm" symbIdRef="eta_Vp"/>
						</math:Uniop>	
					</math:Binop>
				</ct:Assign>
			</IndividualParameter>
			<IndividualParameter symbId="CL">
				<ct:Assign>
					<math:Binop op="times">
						<ct:SymbRef blkIdRef="pm" symbIdRef="pop_CL"/>
						<math:Uniop op="exp">
							<ct:SymbRef blkIdRef="pm" symbIdRef="eta_CL"/>
						</math:Uniop>	
					</math:Binop>
				</ct:Assign>
			</IndividualParameter>
			<IndividualParameter symbId="ka">
				<ct:Assign>
					<ct:SymbRef blkIdRef="pm" symbIdRef="pop_Ka"/>
				</ct:Assign>
			</IndividualParameter>
			<IndividualParameter symbId="Q">
				<ct:Assign>
					<ct:SymbRef blkIdRef="pm" symbIdRef="pop_Q"/>
				</ct:Assign>
			</IndividualParameter>
			<IndividualParameter symbId="MTT">
				<ct:Assign>
					<ct:SymbRef blkIdRef="pm" symbIdRef="pop_MTT"/>
				</ct:Assign>
			</IndividualParameter>
			<IndividualParameter symbId="n">
				<ct:Assign>
					<ct:SymbRef blkIdRef="pm" symbIdRef="pop_n"/>
				</ct:Assign>
			</IndividualParameter>
			<IndividualParameter symbId="kcp">
				<ct:Assign>
					<math:Binop op="divide">
						<ct:SymbRef blkIdRef="pm" symbIdRef="Q"/>
						<ct:SymbRef blkIdRef="pm" symbIdRef="Vc"/>
					</math:Binop>
				</ct:Assign>
			</IndividualParameter>
			<IndividualParameter symbId="kpc">
				<ct:Assign>
					<math:Binop op="divide">
						<ct:SymbRef blkIdRef="pm" symbIdRef="Q"/>
						<ct:SymbRef blkIdRef="pm" symbIdRef="Vp"/>
					</math:Binop>
				</ct:Assign>
			</IndividualParameter>
			<IndividualParameter symbId="kel">
				<ct:Assign>
					<math:Binop op="divide">
						<ct:SymbRef blkIdRef="pm" symbIdRef="CL"/>
						<ct:SymbRef blkIdRef="pm" symbIdRef="Vc"/>
					</math:Binop>
				</ct:Assign>
			</IndividualParameter>
			<IndividualParameter symbId="ktr">
				<ct:Assign>
					<math:Binop op="divide">
						<math:Binop op="plus">
							<ct:SymbRef blkIdRef="pm" symbIdRef="n"/>
							<ct:Int>1</ct:Int>
						</math:Binop>
						<ct:SymbRef blkIdRef="pm" symbIdRef="MTT"/>
					</math:Binop>
				</ct:Assign>
			</IndividualParameter>
			<IndividualParameter symbId="Dose">
				<ct:Assign>
					<math:Piecewise>
						<math:Piece>
							<ct:SymbRef blkIdRef="cm" symbIdRef="AMT"/>
							<math:Condition>
								<math:LogicBinop op="gt">
									<ct:SymbRef blkIdRef="cm" symbIdRef="AMT"/>
									<ct:Int>0</ct:Int>
								</math:LogicBinop>
							</math:Condition>
						</math:Piece>
						<math:Piece>
							<ct:Int>0</ct:Int>
							<math:Condition>
								<math:Otherwise/>
							</math:Condition>
						</math:Piece>
					</math:Piecewise>
				</ct:Assign>
			</IndividualParameter>
			<IndividualParameter symbId="amin">
				<ct:Assign>
					<math:Binop op="min">
						<ct:SymbRef blkIdRef="pm" symbIdRef="Dose"/>
						<ct:SymbRef blkIdRef="pm" symbIdRef="n"/>
					</math:Binop>	
				</ct:Assign>
			</IndividualParameter>
			<IndividualParameter symbId="amax">
				<ct:Assign>
					<math:Binop op="max">
						<ct:SymbRef blkIdRef="pm" symbIdRef="Dose"/>
						<ct:SymbRef blkIdRef="pm" symbIdRef="n"/>
					</math:Binop>	
				</ct:Assign>
			</IndividualParameter>
		</ParameterModel>
		<StructuralModel blkId="sm">
			<ct:Variable symbId="Aader" symbolType="real">
				<ct:Assign>
					<math:Piecewise>
						<math:Piece>
							<ct:Int>0</ct:Int>
							<math:Condition>
								<math:LogicBinop op="eq">
									<ct:SymbRef symbIdRef="T"/>
									<ct:Int>0</ct:Int>
								</math:LogicBinop>
							</math:Condition>
						</math:Piece>
						<math:Piece>
							<math:Binop op="minus">
								<math:Uniop op="exp">
									<math:Binop op="minus">
										<math:Binop op="minus">
											<math:Binop op="plus">
												<math:Binop op="plus">
													<ct:SymbRef blkIdRef="pm" symbIdRef="Dose"/>
													<ct:SymbRef blkIdRef="pm" symbIdRef="ktr"/>
												</math:Binop>
												<math:Binop op="times">
													<ct:SymbRef blkIdRef="pm" symbIdRef="n"/>
													<math:Uniop op="log">
														<math:Binop op="times">
															<ct:SymbRef blkIdRef="pm" symbIdRef="ktr"/>
															<ct:SymbRef symbIdRef="T"/>
														</math:Binop>
													</math:Uniop>	
												</math:Binop>
											</math:Binop>
											<math:Binop op="times">
												<ct:SymbRef blkIdRef="pm" symbIdRef="ktr"/>
												<ct:SymbRef symbIdRef="T"/>
											</math:Binop>
										</math:Binop>
										<math:Uniop op="factln">
											<ct:SymbRef blkIdRef="pm" symbIdRef="n"/>
										</math:Uniop>	
									</math:Binop>
								</math:Uniop>	
								<math:Binop op="times">
									<ct:SymbRef blkIdRef="pm" symbIdRef="ka"/>
									<ct:SymbRef blkIdRef="sm" symbIdRef="Aa"/>
								</math:Binop>
							</math:Binop>
							<math:Condition>
								<math:Otherwise/>
							</math:Condition>
						</math:Piece>
					</math:Piecewise>
				</ct:Assign>
			</ct:Variable>
			<ct:DerivativeVariable symbId="Aa" symbolType="real">
				<ct:Assign>
					<ct:SymbRef blkIdRef="sm" symbIdRef="Aader"/>
				</ct:Assign>
				<ct:IndependentVariable>
					<ct:SymbRef symbIdRef="T"/>
				</ct:IndependentVariable>
				<ct:InitialCondition>
					<ct:InitialValue>
						<ct:Assign>
							<ct:Int>0</ct:Int>
						</ct:Assign>
					</ct:InitialValue>
					<ct:InitialTime>
						<ct:Assign>
							<ct:Int>0</ct:Int>
						</ct:Assign>	
					</ct:InitialTime>
				</ct:InitialCondition>
			</ct:DerivativeVariable>
			<ct:DerivativeVariable symbId="Ac" symbolType="real">
				<ct:Assign>
					<math:Binop op="plus">
						<math:Binop op="minus">
							<math:Binop op="minus">
								<math:Binop op="times">
									<ct:SymbRef blkIdRef="pm" symbIdRef="ka"/>
									<ct:SymbRef blkIdRef="sm" symbIdRef="Aa"/>
								</math:Binop>
								<math:Binop op="times">
									<ct:SymbRef blkIdRef="pm" symbIdRef="kel"/>
									<ct:SymbRef blkIdRef="sm" symbIdRef="Ac"/>
								</math:Binop>
							</math:Binop>
							<math:Binop op="times">
								<ct:SymbRef blkIdRef="pm" symbIdRef="kcp"/>
								<ct:SymbRef blkIdRef="sm" symbIdRef="Ac"/>
							</math:Binop>
						</math:Binop>
						<math:Binop op="times">
							<ct:SymbRef blkIdRef="pm" symbIdRef="kpc"/>
							<ct:SymbRef blkIdRef="sm" symbIdRef="Ap"/>
						</math:Binop>
					</math:Binop>
				</ct:Assign>
				<ct:IndependentVariable>
					<ct:SymbRef symbIdRef="T"/>
				</ct:IndependentVariable>
				<ct:InitialCondition>
					<ct:InitialValue>
						<ct:Assign>
							<ct:Int>0</ct:Int>
						</ct:Assign>
					</ct:InitialValue>
					<ct:InitialTime>
						<ct:Assign>
							<ct:Int>0</ct:Int>
						</ct:Assign>	
					</ct:InitialTime>
				</ct:InitialCondition>
			</ct:DerivativeVariable>
			<ct:DerivativeVariable symbId="Ap" symbolType="real">
				<ct:Assign>
					<math:Binop op="minus">
						<math:Binop op="times">
							<ct:SymbRef blkIdRef="pm" symbIdRef="kcp"/>
							<ct:SymbRef blkIdRef="sm" symbIdRef="Ac"/>
						</math:Binop>
						<math:Binop op="times">
							<ct:SymbRef blkIdRef="pm" symbIdRef="kpc"/>
							<ct:SymbRef blkIdRef="sm" symbIdRef="Ap"/>
						</math:Binop>
					</math:Binop>
				</ct:Assign>
				<ct:IndependentVariable>
					<ct:SymbRef symbIdRef="T"/>
				</ct:IndependentVariable>
				<ct:InitialCondition>
					<ct:InitialValue>
						<ct:Assign>
							<ct:Int>0</ct:Int>
						</ct:Assign>
					</ct:InitialValue>
					<ct:InitialTime>
						<ct:Assign>
							<ct:Int>0</ct:Int>
						</ct:Assign>	
					</ct:InitialTime>
				</ct:InitialCondition>
			</ct:DerivativeVariable>
			<ct:Variable symbId="Cc" symbolType="real">
				<ct:Assign>
					<math:Binop op="divide">
						<ct:SymbRef blkIdRef="sm" symbIdRef="Ac"/>
						<ct:SymbRef blkIdRef="pm" symbIdRef="Vc"/>
					</math:Binop>
				</ct:Assign>
			</ct:Variable>
		</StructuralModel>
		<ObservationModel blkId="om1">
			<ContinuousData>
				<General symbId="Cc_obs">
					<ct:Assign>
						<math:Binop op="plus">
							<ct:SymbRef blkIdRef="sm" symbIdRef="Cc"/>
							<math:Binop op="times">
								<ct:SymbRef blkIdRef="pm" symbIdRef="b"/>
								<ct:SymbRef blkIdRef="pm" symbIdRef="epsilon_Cc"/>
							</math:Binop>
						</math:Binop>
					</ct:Assign>
				</General>
			</ContinuousData>
		</ObservationModel>
	</ModelDefinition>
	<TrialDesign xmlns="http://www.pharmml.org/pharmml/0.8/TrialDesign">
		<ExternalDataSet toolName="NONMEM" oid="nm_ds">
			<ColumnMapping>
				<ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="ID"/>
				<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
			</ColumnMapping>
			<ColumnMapping>
			    <ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="DV"/>
				<ct:SymbRef blkIdRef="om1" symbIdRef="Cc_obs"/>
			</ColumnMapping>
			<ColumnMapping>
				<ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="TIME"/>
				<ct:SymbRef symbIdRef="T"/>
			</ColumnMapping>
			<ColumnMapping>
				<ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="AMT"/>
				<ct:SymbRef blkIdRef="cm" symbIdRef="AMT"/>
			</ColumnMapping>
			<ColumnMapping>
				<ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="RATE"/>
				<ct:SymbRef blkIdRef="cm" symbIdRef="RATE"/>
			</ColumnMapping>
			<ColumnMapping>
				<ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="WT"/>
				<ct:SymbRef blkIdRef="cm" symbIdRef="WT"/>
			</ColumnMapping>
			<DataSet xmlns="http://www.pharmml.org/pharmml/0.8/Dataset">
				<Definition>
					<Column columnId="ID" columnType="id" valueType="int" columnNum="1"/>
					<Column columnId="DV" columnType="dv" valueType="real" columnNum="2"/>
					<Column columnId="TIME" columnType="idv" valueType="real" columnNum="3"/>
					<Column columnId="MDV" columnType="mdv" valueType="int" columnNum="4"/>
					<Column columnId="AMT" columnType="covariate" valueType="real" columnNum="5"/>
					<Column columnId="RATE" columnType="covariate" valueType="real" columnNum="6"/>
					<Column columnId="WT" columnType="covariate" valueType="real" columnNum="7"/>
					<Column columnId="CMT" columnType="cmt" valueType="int" columnNum="8"/>
				</Definition>
				<ExternalFile oid="id">
					<path>drugX_PO_1occ_dataMDL_v1.csv</path>
					<format>CSV</format>
					<delimiter>COMMA</delimiter>
				</ExternalFile>
			</DataSet>
		</ExternalDataSet>
	</TrialDesign>
	<ModellingSteps xmlns="http://www.pharmml.org/pharmml/0.8/ModellingSteps">
		<EstimationStep oid="estimStep_1">
			<ExternalDataSetReference>
				<ct:OidRef oidRef="nm_ds"/>
			</ExternalDataSetReference>
			<ParametersToEstimate>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="pop_Vc"/>
					<InitialEstimate fixed="false">
						<ct:Int>10</ct:Int>
					</InitialEstimate>
					<LowerBound>
						<ct:Int>0</ct:Int>
					</LowerBound>
					<UpperBound>
						<ct:plusInf/>
					</UpperBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="pop_Vp"/>
					<InitialEstimate fixed="false">
						<ct:Int>30</ct:Int>
					</InitialEstimate>
					<LowerBound>
						<ct:Int>0</ct:Int>
					</LowerBound>
					<UpperBound>
						<ct:plusInf/>
					</UpperBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="pop_CL"/>
					<InitialEstimate fixed="false">
						<ct:Int>6</ct:Int>
					</InitialEstimate>
					<LowerBound>
						<ct:Int>0</ct:Int>
					</LowerBound>
					<UpperBound>
						<ct:plusInf/>
					</UpperBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="pop_Ka"/>
					<InitialEstimate fixed="false">
						<ct:Real>1.5</ct:Real>
					</InitialEstimate>
					<LowerBound>
						<ct:Int>0</ct:Int>
					</LowerBound>
					<UpperBound>
						<ct:plusInf/>
					</UpperBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="pop_Q"/>
					<InitialEstimate fixed="false">
						<ct:Int>10</ct:Int>
					</InitialEstimate>
					<LowerBound>
						<ct:Int>0</ct:Int>
					</LowerBound>
					<UpperBound>
						<ct:plusInf/>
					</UpperBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="pop_MTT"/>
					<InitialEstimate fixed="false">
						<ct:Int>1</ct:Int>
					</InitialEstimate>
					<LowerBound>
						<ct:Int>0</ct:Int>
					</LowerBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="pop_n"/>
					<InitialEstimate fixed="false">
						<ct:Int>9</ct:Int>
					</InitialEstimate>
					<LowerBound>
						<ct:Int>0</ct:Int>
					</LowerBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="b"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.15</ct:Real>
					</InitialEstimate>
					<LowerBound>
						<ct:Int>0</ct:Int>
					</LowerBound>
					<UpperBound>
						<ct:plusInf/>
					</UpperBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="omega_Vc"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.16</ct:Real>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="omega_Vp"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.25</ct:Real>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="omega_CL"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.49</ct:Real>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="sigma"/>
					<InitialEstimate fixed="true">
						<ct:Int>1</ct:Int>
					</InitialEstimate>
				</ParameterEstimation>
			</ParametersToEstimate>
			<Operation order="1" opType="generic">
				<Property name="algo">
					<ct:Assign>
						<ct:String>foce</ct:String>
					</ct:Assign>
				</Property>
			</Operation>
		</EstimationStep>
		<mstep:StepDependencies>
			<mstep:Step>
				<ct:OidRef oidRef="estimStep_1"/>
			</mstep:Step>
		</mstep:StepDependencies>
	</ModellingSteps>		
</PharmML>
