<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
  ~
  ~  Licensed under the Apache License, Version 2.0 (the
  ~  "License"); you may not use this file except in
  ~  compliance with the License.  You may obtain a copy of
  ~  the License at
  ~
  ~       http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~  Unless required by applicable law or agreed to in writing,
  ~  software distributed under the License is distributed on
  ~  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~  KIND, either express or implied. See the License for the
  ~  specific language governing permissions and limitations
  ~  under the License.
  ~
  -->

<PharmML
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns="http://www.pharmml.org/pharmml/0.8/PharmML"
	xsi:schemaLocation="http://www.pharmml.org/pharmml/0.8/PharmML http://www.pharmml.org/pharmml/0.8/PharmML"
	xmlns:math="http://www.pharmml.org/pharmml/0.8/Maths"
	xmlns:ct="http://www.pharmml.org/pharmml/0.8/CommonTypes"
	xmlns:ds="http://www.pharmml.org/pharmml/0.8/Dataset"
	xmlns:mdef="http://www.pharmml.org/pharmml/0.8/ModelDefinition"
	xmlns:mstep="http://www.pharmml.org/pharmml/0.8/ModellingSteps"
	xmlns:design="http://www.pharmml.org/pharmml/0.8/TrialDesign"
	writtenVersion="0.8.1">
	<ct:Name>Generated from MDL. MOG ID: warfarin_PK_ODE_mog</ct:Name>
	<IndependentVariable symbId="TIME"/>
	<ct:FunctionDefinition xmlns="http://www.pharmml.org/pharmml/0.8/CommonTypes"
		symbId="combinedError1"
		symbolType="real">
	    <Description>Combined additive ad proportional for 1 epsilon</Description>
	    <FunctionArgument symbolType="real" symbId="additive"/>
	    <FunctionArgument symbolType="real" symbId="proportional"/>
	    <FunctionArgument symbolType="real" symbId="f"/>
	    <Definition>
	        <Assign>
	            <Binop op="plus" xmlns="http://www.pharmml.org/pharmml/0.8/Maths">
	                <ct:SymbRef symbIdRef="additive"/>
	                <Binop op="times">
	                    <ct:SymbRef symbIdRef="proportional"/>
	                    <ct:SymbRef symbIdRef="f"/>
	                </Binop>
	            </Binop>
	        </Assign>
	    </Definition>
	</ct:FunctionDefinition>
	<ModelDefinition xmlns="http://www.pharmml.org/pharmml/0.8/ModelDefinition">
		<VariabilityModel blkId="vm_err" type="residualError">
			<Level symbId="DV"/>
		</VariabilityModel>
		<VariabilityModel blkId="vm_mdl" type="parameterVariability">
			<Level symbId="ID"/>
		</VariabilityModel>
		<CovariateModel blkId="cm">
			<Covariate symbId="WT">
				<Continuous/>
			</Covariate>
			<Covariate symbId="logtWT">
				<Continuous/>
			</Covariate>
		</CovariateModel>
		<ParameterModel blkId="pm">
			<PopulationParameter symbId = "POP_CL"/>
			<PopulationParameter symbId = "POP_VC"/>
			<PopulationParameter symbId = "POP_Q"/>
			<PopulationParameter symbId = "POP_VP"/>
			<PopulationParameter symbId = "POP_KA"/>
			<PopulationParameter symbId = "POP_TLAG"/>
			<PopulationParameter symbId = "POP_BETA_CL_WT"/>
			<PopulationParameter symbId = "POP_BETA_V_WT"/>
			<PopulationParameter symbId = "RUV_PROP"/>
			<PopulationParameter symbId = "RUV_ADD"/>
			<PopulationParameter symbId = "PPV_CL"/>
			<PopulationParameter symbId = "PPV_VC"/>
			<PopulationParameter symbId = "PPV_Q"/>
			<PopulationParameter symbId = "PPV_VP"/>
			<PopulationParameter symbId = "PPV_KA"/>
			<PopulationParameter symbId = "PPV_TLAG"/>
			<PopulationParameter symbId = "RUV_EPS1"/>
			<PopulationParameter symbId = "RHO_CL_VC"/>
			<RandomVariable symbId="eta_PPV_CL">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal1">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Int>0</ct:Int>
							</ct:Assign>
						</Parameter>
						<Parameter name="stdev">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="PPV_CL"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
			<RandomVariable symbId="eta_PPV_VC">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal1">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Int>0</ct:Int>
							</ct:Assign>
						</Parameter>
						<Parameter name="stdev">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="PPV_VC"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
			<RandomVariable symbId="eta_PPV_Q">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal1">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Int>0</ct:Int>
							</ct:Assign>
						</Parameter>
						<Parameter name="stdev">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="PPV_Q"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
			<RandomVariable symbId="eta_PPV_VP">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal1">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Int>0</ct:Int>
							</ct:Assign>
						</Parameter>
						<Parameter name="stdev">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="PPV_VP"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
			<RandomVariable symbId="eta_PPV_KA">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal1">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Int>0</ct:Int>
							</ct:Assign>
						</Parameter>
						<Parameter name="stdev">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="PPV_KA"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
			<RandomVariable symbId="eta_PPV_TLAG">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal1">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Int>0</ct:Int>
							</ct:Assign>
						</Parameter>
						<Parameter name="stdev">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="PPV_TLAG"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
			<RandomVariable symbId="eps_RUV_EPS1">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_err" symbIdRef="DV"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal1">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Int>0</ct:Int>
							</ct:Assign>
						</Parameter>
						<Parameter name="stdev">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="RUV_EPS1"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
			<IndividualParameter symbId="CL">
				<StructuredModel>
					<Transformation type="log" />
					<LinearCovariate>
						<PopulationValue>
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="POP_CL"/>
							</ct:Assign>
						</PopulationValue>
						<Covariate>
							<ct:SymbRef blkIdRef="cm" symbIdRef="logtWT"/>
							<FixedEffect>
								<ct:SymbRef blkIdRef="pm" symbIdRef="POP_BETA_CL_WT"/>
							</FixedEffect>
						</Covariate>
					</LinearCovariate>
					<RandomEffects>
						<ct:SymbRef blkIdRef="pm" symbIdRef="eta_PPV_CL"/>
					</RandomEffects>
				</StructuredModel>
			</IndividualParameter>
			<IndividualParameter symbId="VC">
				<StructuredModel>
					<Transformation type="log" />
					<LinearCovariate>
						<PopulationValue>
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="POP_VC"/>
							</ct:Assign>
						</PopulationValue>
						<Covariate>
							<ct:SymbRef blkIdRef="cm" symbIdRef="logtWT"/>
							<FixedEffect>
								<ct:SymbRef blkIdRef="pm" symbIdRef="POP_BETA_V_WT"/>
							</FixedEffect>
						</Covariate>
					</LinearCovariate>
					<RandomEffects>
						<ct:SymbRef blkIdRef="pm" symbIdRef="eta_PPV_VC"/>
					</RandomEffects>
				</StructuredModel>
			</IndividualParameter>
			<IndividualParameter symbId="Q">
				<StructuredModel>
					<Transformation type="log" />
					<LinearCovariate>
						<PopulationValue>
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="POP_Q"/>
							</ct:Assign>
						</PopulationValue>
					</LinearCovariate>
					<RandomEffects>
						<ct:SymbRef blkIdRef="pm" symbIdRef="eta_PPV_Q"/>
					</RandomEffects>
				</StructuredModel>
			</IndividualParameter>
			<IndividualParameter symbId="VP">
				<StructuredModel>
					<Transformation type="log" />
					<LinearCovariate>
						<PopulationValue>
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="POP_VP"/>
							</ct:Assign>
						</PopulationValue>
					</LinearCovariate>
					<RandomEffects>
						<ct:SymbRef blkIdRef="pm" symbIdRef="eta_PPV_VP"/>
					</RandomEffects>
				</StructuredModel>
			</IndividualParameter>
			<IndividualParameter symbId="KA">
				<StructuredModel>
					<Transformation type="log" />
					<LinearCovariate>
						<PopulationValue>
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="POP_KA"/>
							</ct:Assign>
						</PopulationValue>
					</LinearCovariate>
					<RandomEffects>
						<ct:SymbRef blkIdRef="pm" symbIdRef="eta_PPV_KA"/>
					</RandomEffects>
				</StructuredModel>
			</IndividualParameter>
			<IndividualParameter symbId="TLAG">
				<StructuredModel>
					<Transformation type="log" />
					<LinearCovariate>
						<PopulationValue>
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="POP_TLAG"/>
							</ct:Assign>
						</PopulationValue>
					</LinearCovariate>
					<RandomEffects>
						<ct:SymbRef blkIdRef="pm" symbIdRef="eta_PPV_TLAG"/>
					</RandomEffects>
				</StructuredModel>
			</IndividualParameter>
			<IndividualParameter symbId="ALAG1">
				<ct:Assign>
					<ct:SymbRef blkIdRef="pm" symbIdRef="TLAG"/>
				</ct:Assign>
			</IndividualParameter>
			<IndividualParameter symbId="V2">
				<ct:Assign>
					<ct:SymbRef blkIdRef="pm" symbIdRef="VC"/>
				</ct:Assign>
			</IndividualParameter>
			<IndividualParameter symbId="V3">
				<ct:Assign>
					<ct:SymbRef blkIdRef="pm" symbIdRef="VP"/>
				</ct:Assign>
			</IndividualParameter>
			<IndividualParameter symbId="S2">
				<ct:Assign>
					<ct:SymbRef blkIdRef="pm" symbIdRef="VC"/>
				</ct:Assign>
			</IndividualParameter>
			<Correlation>
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
				</ct:VariabilityReference>
				<Pairwise>
					<RandomVariable1>
						<ct:SymbRef symbIdRef="eta_PPV_CL"/>
					</RandomVariable1>
					<RandomVariable2>
						<ct:SymbRef symbIdRef="eta_PPV_VC"/>
					</RandomVariable2>
					<CorrelationCoefficient>
						<ct:Assign>
							<ct:SymbRef blkIdRef="pm" symbIdRef="RHO_CL_VC"/>
						</ct:Assign>
					</CorrelationCoefficient>
				</Pairwise>
			</Correlation>
		</ParameterModel>
		<StructuralModel blkId="sm">
			<ct:Variable symbId="F" symbolType="real">
				<ct:Assign>
					<math:Binop op="divide">
						<ct:SymbRef blkIdRef="sm" symbIdRef="CENTRAL"/>
						<ct:SymbRef blkIdRef="pm" symbIdRef="S2"/>
					</math:Binop>
				</ct:Assign>
			</ct:Variable>
			<ct:Variable symbId="CC" symbolType="real">
				<ct:Assign>
					<ct:SymbRef blkIdRef="sm" symbIdRef="F"/>
				</ct:Assign>
			</ct:Variable>
			<ct:Variable symbId="CENTRAL" symbolType="real"/>
			<ct:Variable symbId="PERIPHERAL" symbolType="real"/>
			<PKmacros>
				<Absorption>
					<Value argument="adm"> 
						<ct:Int>1</ct:Int>
					</Value>
					<Value argument="cmt"> 
						<ct:Int>2</ct:Int>
					</Value>
					<Value argument="ka"> 
						<ct:Assign>
							<ct:SymbRef blkIdRef="pm" symbIdRef="KA"/>
						</ct:Assign>
					</Value>
					<Value argument="Tlag"> 
						<ct:Assign>
							<ct:SymbRef blkIdRef="pm" symbIdRef="ALAG1"/>
						</ct:Assign>
					</Value>
				</Absorption>
				<Compartment>
					<Value argument="amount"> 
						<ct:SymbRef blkIdRef="sm" symbIdRef="CENTRAL"/>
					</Value>
					<Value argument="cmt"> 
						<ct:Int>2</ct:Int>
					</Value>
				</Compartment>
				<Elimination>
					<Value argument="cmt"> 
						<ct:Int>2</ct:Int>
					</Value>
					<Value argument="V"> 
						<ct:Assign>
							<ct:SymbRef blkIdRef="pm" symbIdRef="V2"/>
						</ct:Assign>
					</Value>
					<Value argument="CL"> 
						<ct:Assign>
							<ct:SymbRef blkIdRef="pm" symbIdRef="CL"/>
						</ct:Assign>
					</Value>
				</Elimination>
				<Peripheral>
					<Value argument="amount"> 
						<ct:SymbRef blkIdRef="sm" symbIdRef="PERIPHERAL"/>
					</Value>
					<Value argument="k_2_3"> 
						<ct:Assign>
							<math:Binop op="divide">
								<ct:SymbRef blkIdRef="pm" symbIdRef="Q"/>
								<ct:SymbRef blkIdRef="pm" symbIdRef="V2"/>
							</math:Binop>
						</ct:Assign>
					</Value>
					<Value argument="k_3_2"> 
						<ct:Assign>
							<math:Binop op="divide">
								<ct:SymbRef blkIdRef="pm" symbIdRef="Q"/>
								<ct:SymbRef blkIdRef="pm" symbIdRef="V3"/>
							</math:Binop>
						</ct:Assign>
					</Value>
				</Peripheral>
			</PKmacros>
		</StructuralModel>
		<ObservationModel blkId="om1">
			<ContinuousData>
				<Standard symbId="CC_obs">
					<Output>
						<ct:SymbRef blkIdRef="sm" symbIdRef="CC"/>
					</Output>
					<ErrorModel>
						<ct:Assign>
							<math:FunctionCall>
								<ct:SymbRef symbIdRef="combinedError1"/>
								<math:FunctionArgument symbId="additive">
									<ct:SymbRef blkIdRef="pm" symbIdRef="RUV_ADD"/>
								</math:FunctionArgument>
								<math:FunctionArgument symbId="proportional">
									<ct:SymbRef blkIdRef="pm" symbIdRef="RUV_PROP"/>
								</math:FunctionArgument>
								<math:FunctionArgument symbId="f">
									<ct:SymbRef blkIdRef="sm" symbIdRef="CC"/>
								</math:FunctionArgument>
							</math:FunctionCall>
						</ct:Assign>
					</ErrorModel>
					<ResidualError>
						<ct:SymbRef blkIdRef="pm" symbIdRef="eps_RUV_EPS1"/>
					</ResidualError>
				</Standard>
			</ContinuousData>
		</ObservationModel>
	</ModelDefinition>
	<TrialDesign xmlns="http://www.pharmml.org/pharmml/0.8/TrialDesign">
		<ExternalDataSet toolName="Monolix" oid="nm_ds">
			<ColumnMapping>
				<ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="ID"/>
				<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
			</ColumnMapping>
			<ColumnMapping>
				<ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="TIME"/>
				<ct:SymbRef symbIdRef="TIME"/>
			</ColumnMapping>
			<ColumnMapping>
				<ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="WT"/>
				<ct:SymbRef blkIdRef="cm" symbIdRef="WT"/>
			</ColumnMapping>
			<ColumnMapping>
				<ds:ColumnRef columnIdRef="AMT"/>
				<ds:TargetMapping blkIdRef="sm">
					<ds:Map dataSymbol="1" admNumber="1"/>
				</ds:TargetMapping>
			</ColumnMapping>
			<ColumnMapping>
			    <ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="DV"/>
				<ct:SymbRef blkIdRef="om1" symbIdRef="CC_obs"/>
			</ColumnMapping>
			<ColumnMapping>
				<ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="logtWT"/>
				<ct:SymbRef blkIdRef="cm" symbIdRef="logtWT"/>
			</ColumnMapping>
			<DataSet xmlns="http://www.pharmml.org/pharmml/0.8/Dataset">
				<Definition>
					<Column columnId="ID" columnType="id" valueType="int" columnNum="1"/>
					<Column columnId="TIME" columnType="idv" valueType="real" columnNum="2"/>
					<Column columnId="WT" columnType="covariate" valueType="real" columnNum="3"/>
					<Column columnId="AMT" columnType="dose" valueType="real" columnNum="4"/>
					<Column columnId="CMT" columnType="adm" valueType="int" columnNum="5"/>
					<Column columnId="DVID" columnType="dvid" valueType="int" columnNum="6"/>
					<Column columnId="DV" columnType="dv" valueType="real" columnNum="7"/>
					<Column columnId="MDV" columnType="mdv" valueType="int" columnNum="8"/>
					<Column columnId="logtWT" columnType="covariate" valueType="real" columnNum="9"/>
				</Definition>
				<ExternalFile oid="id">
					<path>warfarin_conc_cmt.csv</path>
					<format>CSV</format>
					<delimiter>COMMA</delimiter>
				</ExternalFile>
			</DataSet>
		</ExternalDataSet>
	</TrialDesign>
	<ModellingSteps xmlns="http://www.pharmml.org/pharmml/0.8/ModellingSteps">
		<EstimationStep oid="estimStep_1">
			<ExternalDataSetReference>
				<ct:OidRef oidRef="nm_ds"/>
			</ExternalDataSetReference>
			<ParametersToEstimate>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="POP_CL"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.1</ct:Real>
					</InitialEstimate>
					<LowerBound>
						<ct:Real>0.001</ct:Real>
					</LowerBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="POP_VC"/>
					<InitialEstimate fixed="false">
						<ct:Int>8</ct:Int>
					</InitialEstimate>
					<LowerBound>
						<ct:Real>0.001</ct:Real>
					</LowerBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="POP_Q"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.2</ct:Real>
					</InitialEstimate>
					<LowerBound>
						<ct:Real>0.001</ct:Real>
					</LowerBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="POP_VP"/>
					<InitialEstimate fixed="false">
						<ct:Int>20</ct:Int>
					</InitialEstimate>
					<LowerBound>
						<ct:Real>0.001</ct:Real>
					</LowerBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="POP_KA"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.362</ct:Real>
					</InitialEstimate>
					<LowerBound>
						<ct:Real>0.001</ct:Real>
					</LowerBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="POP_TLAG"/>
					<InitialEstimate fixed="false">
						<ct:Int>1</ct:Int>
					</InitialEstimate>
					<LowerBound>
						<ct:Real>0.001</ct:Real>
					</LowerBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="POP_BETA_CL_WT"/>
					<InitialEstimate fixed="true">
						<ct:Real>0.75</ct:Real>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="POP_BETA_V_WT"/>
					<InitialEstimate fixed="true">
						<ct:Int>1</ct:Int>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="RUV_PROP"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.1</ct:Real>
					</InitialEstimate>
					<LowerBound>
						<ct:Int>0</ct:Int>
					</LowerBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="RUV_ADD"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.1</ct:Real>
					</InitialEstimate>
					<LowerBound>
						<ct:Int>0</ct:Int>
					</LowerBound>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="PPV_CL"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.1</ct:Real>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="PPV_VC"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.1</ct:Real>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="PPV_Q"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.1</ct:Real>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="PPV_VP"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.1</ct:Real>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="PPV_KA"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.1</ct:Real>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="PPV_TLAG"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.1</ct:Real>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="RUV_EPS1"/>
					<InitialEstimate fixed="true">
						<ct:Int>1</ct:Int>
					</InitialEstimate>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="RHO_CL_VC"/>
					<InitialEstimate fixed="false">
						<ct:Real>0.01</ct:Real>
					</InitialEstimate>
				</ParameterEstimation>
			</ParametersToEstimate>
			<Operation order="1" opType="generic">
				<Property name="algo">
					<ct:Assign>
						<ct:String>saem</ct:String>
					</ct:Assign>
				</Property>
			</Operation>
		</EstimationStep>
		<mstep:StepDependencies>
			<mstep:Step>
				<ct:OidRef oidRef="estimStep_1"/>
			</mstep:Step>
		</mstep:StepDependencies>
	</ModellingSteps>		
</PharmML>
